# FIRopt

This tool designs linear discrete digital filters with Finite Impulse Response (FIR).
This project aims at fusion of the filter design with the design of dedicated hardware architecture.

**Context**: multiplierless implementations of products by constants using shift-and-add architectures. 
**Problem**: determine the coefficients of an FIR filter in fixed-point arithmetic such that the total cost of the shift-and-add archutecture is minimal in terms of number of adders. 

**Techniques**: our approach is based on a formalisation of the problem as an instance of a Mixed Integer Linear Programming problem, which is asubsequently solved using a third-party solver (see **Built With** section). Full description can be found in the research report available [here]().

**Input**: 
* frequency specifications 
* filter type and order
* wordlength

**Output**:
* filter coefficients 
* adder graph structure for the optimal solution
   
**Guarantees** (in case of successful exit):
* optimality of the proposed solution
* a posteriori validation of frequency specifications

**Options** (for a full list see **Interface** section):
* bounded/unbounded adder depth
* variable/fixed gain
* uniform or Fekete-point based frequency grid discretization
* pre-loading of an initial feasible solution
* ...

    
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

This tool has following prerequisities: 

**Required:** 
* [CMake](https://cmake.org) v3.12 or later 
* [ScaLP](https://digidev.digi.e-technik.uni-kassel.de/scalp/wiki/WikiStart) - C++ library that provides wrappers for different LP solvers
* [Eigen](http://eigen.tuxfamily.org/) - C++ template library for linear algebra operations, v3.3 or later

**Solvers** (must use **at least one** of them):
* [Gurobi](http://www.gurobi.com) - commercial ILP solver, has free academic licence
* [CPLEX](https://www.ibm.com/analytics/cplex-optimizer) - commercial ILP solver by IBM, has free academic licence 
* [SCIP](https://scip.zib.de) - non-commercial ILP solver


**Optional:**
* [PAGSuite](https://digidev.digi.e-technik.uni-kassel.de/pagsuite/) - the paglib library is used if loading an initial feasible adder graph solution

#### Installation order
We suggest the following installation order:
1. One/several solvers (for details on the installaiton of each solver see their respective documentation)
2. ScaLP suite (details on building ScaLP with one/several solvers can be found [here](https://digidev.digi.e-technik.uni-kassel.de/scalp/wiki/DeveloperDocumentation))
3. Eigen library 
4. Optionally, PAGSuite

**Quick-start tip:** the authors find that the quickest solver to install is Gurobi.

**Quick-start tip:** on MacOS X the eigen library can be installed using the Homebrew package manager:
```
brew install eigen
```
Eigen is essentially a collection of header files. Typically, the above command will make them available in
```
/usr/local/include/
```
which should be on the standard search path on most systems.

### Compiling the tool

In the following we assume that:
* FIROPT_PREFIX is the path to the project folder
* ScaLP is installed in SCALP_PREFIX_PATH
    * Tip: if ScaLP is not installed in a standard location, provide the path to the ScaLP library as SCALP_LIB and for ScaLP headers as SCALP_H
* Eigen headers are installed in EIGEN_INCLUDE_PATH

This is a CMake-based project, hence the installation starts with
```
cd FIROPT_PREFIX
mkdir build
cd build
```
Typical configuration line is (if ScaLP and/or Eigen are not in standard locations)
```
cmake ../ -DSCALP_PREFIX_PATH=$SCALP_PREFIX_PATH -DEIGEN_INCLUDE_PATH=$EIGEN_INCLUDE_PATH
```
For general configuration options see cmake help.

Compilation is as simple as 
```
make 
```


## Interface of the tool
### Input
The tool proposes a command-line interface where the problem and option specifications are given as command-line arguments.
The full list of options and their usage is available via help:
```
./firopt --help
```
Some of the essential options for a quick-start:
```
-----------------------------------------------
General Options:
-----------------------------------------------
Option                                          Meaning
--wordlength=[int]                              Coefficient wordlength (default: 8 bit)
--noOfCoefficients=[int]                        Number of coefficients (default: 13)
--ftype=[1|2|3|4]                               Filter Type. 1:SYM_EVEN, 2:SYM_ODD, 3:ASYM_EVEN, 4:ASYM_ODD (default: 1)
--gain=[fixed|flexible]                         Determines if the overall gain should be fixed or kept open as a further degree of freedom in the optimization (default: fixed)
--gridPoints=[int]                              Sets the number of grid points (default: 40)
--solver=[Gurobi|CPLEX|SCIP|LPSolve]            ILP solver (if available in ScaLP library), also a comma separated wish list of solvers can be provided
--output_name=[string]                          When specified, uses [string] as the root name for all tool output files
--objective=[A|SA|MA]                           Selects the objective to be the number of adders (A), structural adders only (SA) and multiplier block adders only (MA) (default: A)
--stages=[int]                                  Number of pipeline stages for ILP2 (default: -1 - auto-detection from wordlength)
-----------------------------------------------
Filter Specification:
-----------------------------------------------
Option                                          Meaning
--fbands=[[float],[float]],[[float],[float]]... Explicit declaration of fbands
--abands=[[float],[float]],[[float],[float]]... Explicit declaration of abands
--desired_bands=[int],[int]...                  Autocompute abands using desired bands and delta
--delta=[float]                                 Alternative declaration of delta. Uniform delta for all abands. + and - will be autocomputed.
```


### Output 
The tool typically produces a more or less comprehensive report to the terminal's output (this can be controlled via the --verbose flag) and several output files:
* output_name.lp -- the LP problem formulation in the CPLEX LP format
* output_name.settings -- problem settings with the list of all FIRopt parameters
* output_name.m -- a MATLAB script containing the result of the design (ftype, wordlength, fbands, abands, filter coefficients and a pipeline set)

### Interface tips and tricks

* Filter *order* should be converted into the *number of coefficients*. For example, a type-1 N-th order filter has 1 + N/2 coefficients.
* Amplitude in decibels (dB) must be converted to gain via y = 10^(x dB / 20). For example, amplitude -20dB is equivalent to the gain 0.1. 
* Frequency specifications can be passed to the tool in various forms. The following ones are equivalent:
    ```
    --fbands=[0, 0.3],[0.5, 1] --abands=[0.9, 1.1]
    ```
    ```
    --fbands=[0, 0.3],[0.5, 1] --desired_bands=1,0 --delta=0.1 
    ```
     ```
    --fbands=[0, 0.3],[0.5, 1] --desired_bands=1,0 --delta=0.1,0.1
    ```
* We suggest to always set an output name for the filter

## Examples

### A simple fixed-gain lowpass filter
Consider the following problem: design a type-1 (symmetric even) 8-th order filter with a normalized passband in [0, 0.3] and stopband in [0.5, 1]; maximum passband ripple and stopband attenuation are -20dB. The coefficient wordlength is 8 bits,  the maximum adder graph depth is 2 stages and the filter gain is fixed. 


**The command-line call:**
```
./firopt --solver=Gurobi --wordlength=8 --ftype=1 --formulation=2 --fbands=[0, 0.3],[0.5, 1] --desired_bands=1,0 --delta=.1  --noOfCoefficients=5  --gain=fixed --output_name=simple_filter --verbose=0
```

**The output:**
<details>
  <summary>**Click to see**</summary>
  
  ```
  ...
Warning for adding constraints: zero or small (< 1e-13) coefficients, ignored
Optimize a model with 364 rows, 488 columns and 2965 nonzeros
Variable types: 146 continuous, 342 integer (342 binary)
Coefficient statistics:
  Matrix range     [5e-02, 5e+02]
  Objective range  [1e+00, 1e+00]
  Bounds range     [1e+00, 5e+01]
  RHS range        [1e+00, 5e+02]
Presolve removed 129 rows and 302 columns
Presolve time: 0.01s
Presolved: 235 rows, 186 columns, 1116 nonzeros
Variable types: 21 continuous, 165 integer (158 binary)
Found heuristic solution: objective 5.0000000
Presolve removed 1 rows and 1 columns
Presolved: 234 rows, 185 columns, 1114 nonzeros


Root relaxation: objective 2.000000e+00, 56 iterations, 0.00 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0    2.00000    0    5    5.00000    2.00000  60.0%     -    0s
H    0     0                       2.0000000    2.00000  0.00%     -    0s

Explored 1 nodes (237 simplex iterations) in 0.03 seconds
Thread count was 4 (of 4 available processors)

Solution count 2: 2 5

Optimal solution found (tolerance 1.00e-04)
Best objective 2.000000000000e+00, best bound 2.000000000000e+00, gap 0.0000%
type=1
greedyCalls=0
The result is OPTIMAL
filter_order=8
number_of_adders=10
number_of_mult_adders=2
number_of_struct_adders=8
pipeline_set={[1 ][1 3 5 ]}
h=[96 80 20 -16 -16]
Started naive validation...
Gain value: 1
Wordlength: 8
naive_validation=OK
gain=1
  ```
</details>


The obtained solution is a filter with an impulse response h(k)
```
-16 -16 20 80 96 80 20 -16 -16
```
that can be realized with 10 adders: 8 structural and 2 multiplier adders. 
The adder graph is described via the two-stage pipeline_set {[1][1 3 5 ]}.

### A simple flexible-gain lowpass filter

Setting the gain in the above example to flexible yields a completely different solution with the total cost 9 adders.


**The command-line call:**
```
 ./firopt --solver=Gurobi --ftype=1 --formulation=2 --fbands=[0, 0.3],[0.5, 1] --desired_bands=1,0 --delta=.1  --noOfCoefficients=5 --gridPoints=40 --wordlength=8 --output_name=simple_filter --gain=flexible --verbose=0
```

**The output:**
<details>
  <summary>**Click to see**</summary>
  
  ```
...

Warning for adding constraints: zero or small (< 1e-13) coefficients, ignored
Optimize a model with 545 rows, 912 columns and 6251 nonzeros
Variable types: 115 continuous, 797 integer (797 binary)
Coefficient statistics:
  Matrix range     [5e-02, 5e+02]
  Objective range  [1e+00, 1e+00]
  Bounds range     [7e-01, 9e+01]
  RHS range        [7e-01, 5e+02]
Presolve removed 128 rows and 536 columns
Presolve time: 0.04s
Presolved: 417 rows, 376 columns, 2619 nonzeros
Variable types: 1 continuous, 375 integer (370 binary)
Presolve removed 26 rows and 22 columns
Presolved: 391 rows, 354 columns, 2563 nonzeros


Root relaxation: objective 0.000000e+00, 84 iterations, 0.00 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0    0.00000    0    6          -    0.00000      -     -    0s
     0     0    0.09203    0   25          -    0.09203      -     -    0s
     0     0    0.09610    0   25          -    0.09610      -     -    0s
     0     0    0.17394    0   16          -    0.17394      -     -    0s
     0     0    0.18027    0   25          -    0.18027      -     -    0s
     0     0    0.21573    0   27          -    0.21573      -     -    0s
H    0     0                       2.0000000    0.21573  89.2%     -    0s
     0     0    0.21666    0   29    2.00000    0.21666  89.2%     -    0s
     0     0    0.33333    0    9    2.00000    0.33333  83.3%     -    0s
     0     0    0.33333    0   11    2.00000    0.33333  83.3%     -    0s
     0     0    0.33333    0   11    2.00000    0.33333  83.3%     -    0s
     0     0    0.33333    0    7    2.00000    0.33333  83.3%     -    0s
     0     0    0.33333    0   23    2.00000    0.33333  83.3%     -    0s
     0     0    0.33333    0    8    2.00000    0.33333  83.3%     -    0s
     0     0    0.33333    0   18    2.00000    0.33333  83.3%     -    0s
     0     0    1.00000    0   12    2.00000    1.00000  50.0%     -    0s
     0     0    1.00000    0   15    2.00000    1.00000  50.0%     -    0s
     0     0    1.00000    0    6    2.00000    1.00000  50.0%     -    0s
     0     0    1.00000    0    3    2.00000    1.00000  50.0%     -    0s
H    0     0                       1.0000000    1.00000  0.00%     -    0s

Cutting planes:
  Gomory: 1
  GUB cover: 1

Explored 1 nodes (2142 simplex iterations) in 0.39 seconds
Thread count was 4 (of 4 available processors)

Solution count 2: 1 2

Optimal solution found (tolerance 1.00e-04)
Best objective 1.000000000000e+00, best bound 1.000000000000e+00, gap 0.0000%
type=1
greedyCalls=0
The result is OPTIMAL
filter_order=8
number_of_adders=9
number_of_mult_adders=1
number_of_struct_adders=8
pipeline_set={[1 3 ][1 3 ]}
h=[128 96 32 -16 -24]
Started naive validation...
Gain value: 1.25084441189999751031791674904525279998779296875
Wordlength: 8
Overflow at 0.1893489 with error = 0.0002954488
Changing gain from 1.250844 to 1.25118
naive_validation=OK
gain=1.2511803760199793345009311451576650142669677734375

```

</details>



**NOTE:** the naive filter validation with the initial gain did not pass due to numerical issues related to the rounding. Our tool automatically determined this fact and adjusted the gain value such that the a posteriori frequency validation passes without any changes to the filter coefficients or the pipeline-set. 

### Other examples
Shell scripts for designing state-of-the-art filters from literature can be found in the /examples/ folder. Numerical results are described in the [research report](). 


## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/filteropt/firopt/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.


## Authors
In alphabetical order:
* **Silviu Filip** - *Initial work* 
* **Martin Kumm** - *Initial work* 
* **Anastasia Volkova** - *Initial work* 

See also the list of [contributors](https://gitlab.com/filteropt/firopt/blob/master/CONTRIBUTING.md) who participated in this project.

## License

This project is licensed under the GPL v3.0 License - see the [LICENSE](LICENSE) file for details.