#!/usr/bin/env bash

# $1 -- wordlength
# $2 -- attenuation_start
# $3 -- attenuation_end
# $4 -- filter_order_start
# $5 -- factor for the gridPoints=factor*noOfCoefficients
# $6 -- test_name
# $7 -- "restart" option
# $8 -- other options to be added to ./firopt

#example of usage:
#./redmill_table.sh 8 -2 -50 2 16 redmill-8-2-50-2-greedy-fixed-16n no-restart --greedy=true --gain=fixed --solver=Gurobi"

FIROPT_PATH="./build/"

test_name=$6

cd ${FIROPT_PATH}
mkdir -p ${test_name}

RESULT_PATH="./${test_name}"


rm -f ${RESULT_PATH}/*.txt

RESULT_FILE="${RESULT_PATH}/results.txt"
date -u > ${RESULT_FILE}

#Header plot
#prepare the plot file
PLOT_FILE="${RESULT_PATH}/${test_name}_plot.gp"
echo "# Set the output file type" > ${PLOT_FILE}
#echo "set terminal postscript eps enhanced color solid colortext 9" >> ${PLOT_FILE}
echo "set terminal epslatex standalone" >> ${PLOT_FILE}
echo "set output '${test_name}.tex'" >>${PLOT_FILE}
#echo "# Set the output file name" >> ${PLOT_FILE}
#echo "set output '${test_name}.eps'" >> ${PLOT_FILE}
echo " "  >> ${PLOT_FILE}
echo "set xrange [0:45]" >> ${PLOT_FILE}
echo "set yrange [-50:-2]" >> ${PLOT_FILE}
echo "set xtics 2" >> ${PLOT_FILE}
echo "set ytics 5" >> ${PLOT_FILE}
echo "set style data linespoints" >> ${PLOT_FILE}


#general settings
timeout=7200
ftype=1
#gain="flexible"
#grid="nonuniform"
#gridPoints=160
factor=$5
otherOptions=$8
#solver="Gurobi"
formulation=2

#stages=2

#filter specifications
fbands="[0,0.3],[0.5,1]"
desired_bands="1,0"

#ripple_start=3 #3dB ripple in the passband
attenuation_start=$2
attenuation_end=$3

wordlength=$1
filter_order=$4 # we start with a user-given order
plotdataname="${RESULT_PATH}/plotdata_filterOrder_${filter_order}.txt"

#some flag variables for pretty gnuplots
firstRun=1
colour=1
lastDegreePlotted=0


attenuation=${attenuation_start}
while [ ${attenuation} -ge ${attenuation_end} ];
do

    noOfCoefficients=$((1 + filter_order/2))
    gridPoints=$((factor * noOfCoefficients))
    attenuation_current=$(echo "e((${attenuation}/20) * l(10))" | bc -l)

    delta="${attenuation_current},${attenuation_current}"

    filename="${RESULT_PATH}/${test_name}_noOfCoefficients${noOfCoefficients}_${attenuation}db.log"
    outputName="${RESULT_PATH}/${test_name}_noOfCoefficients${noOfCoefficients}_${attenuation}db.out"

    # if [ "$8" -eq "no-relaxations" ]; then
    #     relaxations="--no-relaxations"
    # else
    #     relaxations=""
    # fi

    echo "./firopt --gridPoints=${gridPoints} --wordlength=${wordlength} ${otherOptions} --ftype=${ftype} --formulation=${formulation} --timeout=${timeout} --fbands=${fbands} --desired_bands=${desired_bands} --delta=${delta} --noOfCoefficients=${noOfCoefficients} --output_name=${outputName}  >  ${filename} "
    time_start=$(date +'%s')
    ./firopt --gridPoints=${gridPoints} --wordlength=${wordlength} ${otherOptions} --ftype=${ftype} --formulation=${formulation} --timeout=${timeout} --fbands=${fbands} --desired_bands=${desired_bands} --delta=${delta} --noOfCoefficients=${noOfCoefficients}  --output_name=${outputName}  >  ${filename}
    exitCode=$?
    time_elapsed=$(($(date +'%s') - $time_start))
    greedyCalls=`awk -F= -v key="greedyCalls" '$1==key {print $2}' ${filename}`
    #echo "------> Exit code was ${exitCode} "
    if [ "${exitCode}" -eq "0" ]; then
        echo "... DONE"
        total_adders=`awk -F= -v key="number_of_adders" '$1==key {print $2}' ${filename}`
        #validation=`awk -F= -v key="naive_validation" '$1==key {print $2}' ${filename}`
        #filter_order=`awk -F= -v key="filter_order" '$1==key {print $2}' ${filename}`

        printf "attenuation=${attenuation}dB \t filter_order=${filter_order} \t noOfCoefficients=${noOfCoefficients} \t total_adders=${total_adders} \t greedyCalls=${greedyCalls} \t time=${time_elapsed} \n" >> ${RESULT_FILE}

        echo "${attenuation} ${total_adders}" >> ${plotdataname}

        #    echo "FIRopt exited with success but naive filter validation failed!"
        #    printf "attenuation=${attenuation}dB \t filter_order=${filter_order} \t total_adders=CHECK_FAILED \n" >> ${RESULT_FILE}
        #    echo " " >> "${RESULT_PATH}/result_filterOrder_${filter_order}.txt"
        #fi
        total_adders=""
        #validation=""

        attenuation=$((attenuation-1))
    else
        if [ "${exitCode}" -eq "10" ]; then
            total_adders=`awk -F= -v key="number_of_adders" '$1==key {print $2}' ${filename}`
            printf "attenuation=${attenuation}dB \t filter_order=${filter_order} \t noOfCoefficients=${noOfCoefficients} \t  \t greedyCalls=${greedyCalls} \t time=${time_elapsed} \t TF CHECK FAILED \n" >> ${RESULT_FILE}
        fi
if [ "${exitCode}" -eq "16" ]; then
total_adders=`awk -F= -v key="number_of_adders" '$1==key {print $2}' ${filename}`
printf "attenuation=${attenuation}dB \t filter_order=${filter_order} \t noOfCoefficients=${noOfCoefficients} \t total_adders=${total_adders} \t greedyCalls=${greedyCalls} \t time=${time_elapsed} \t LPCoeffBound FAILED \n" >> ${RESULT_FILE}
fi

        echo "------->"
        echo "Could not find the solution for attenuation ${attenuation} and filter order ${filter_order}"
        echo "Exit code = ${exitCode}. Increasing the filter order to $((filter_order+1))"
        echo "<-------"
        #writing the plot call to the plot file
        if [ ${firstRun} -eq 1 ]; then
            printf " plot '${plotdataname}' using 2:1 w lp lt 5  lc ${colour} title 'Filter order ${filter_order}'," >> ${PLOT_FILE}
            printf " plot '${plotdataname}' using 2:1 w lp lt 5  lc ${colour} title 'Filter order ${filter_order}',"
            lastDegreePlotted=${filter_order}
            firstRun=0
            colour=$((colour+1))
        else
            lt=$((5 + 2 * ((${lastDegreePlotted} % 2)) ))
                printf " '${plotdataname}' using 2:1 w lp lt ${lt} lc ${colour}  title 'Filter order ${filter_order}'," >> ${PLOT_FILE}
            lastDegreePlotted=${filter_order}
            colour=$((colour+1))
        fi

        filter_order=$((filter_order+1))
        if [ "$7" == "restart" ]; then
            echo "\n restarting the attenuation \n "
            attenuation=${attenuation_start}
        fi

        plotdataname="${RESULT_PATH}/plotdata_filterOrder_${filter_order}.txt"
        echo "#data file for the filter order ${filter_order}" > ${plotdataname}

    fi

done

if [ "${lastDegreePlotted}" -lt "${filter_order}" ]; then

    if [ ${firstRun} -eq 1 ]; then
        printf " plot '${plotdataname}'  using 2:1 w lp lt 5 lc ${colour} title 'Filter order ${filter_order}'" >> ${PLOT_FILE}
        firstRun=0
        printf " plot '${plotdataname}' using 2:1 w lp lt 5  lc ${colour} title 'Filter order ${filter_order}',"
    else
        lt=$((5 + 2 * ((${lastDegreePlotted} % 2)) ))
        printf " '${plotdataname}' using 2:1 w lp lt ${lt} lc ${colour}  title 'Filter order ${filter_order}'" >> ${PLOT_FILE}
    fi
fi


gnuplot ${PLOT_FILE}
