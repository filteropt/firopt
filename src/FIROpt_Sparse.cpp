//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "FIROpt_Sparse.h"

#include <ScaLP/Exception.h>
#include <ScaLP/Solver.h>
#include <ScaLP/SolverDynamic.h>
#include <fstream>

using std::cout;
using std::endl;
using std::list;
using std::ofstream;
using std::pair;
using std::string;
using std::stringstream;

FIROpt_Sparse::FIROpt_Sparse(list<string> solverWishList, int threads,
                             int timeout, int noOfCoefficients, IRtype ir_type,
                             string outputRootName, double_pair_vec_t Fbands,
                             double_pair_vec_t Abands,
                             double_pair_t gain_bounds, int wordlength,
                             int grid_points, int shiftMax, int coeffMax,
                             long bigM, double intFeasTol,
                             bool useIndicatorConstraints)

    : FIROpt(solverWishList, threads, timeout, noOfCoefficients, ir_type,
             outputRootName, Fbands, Abands, wordlength, grid_points,
             gain_bounds)

{

  this->useIndicatorConstraints = useIndicatorConstraints;
  this->intFeasTol = intFeasTol;
  this->shiftMax = shiftMax;
  this->coeffMax = coeffMax;
  this->bigM = bigM;
}

void FIROpt_Sparse::initialize_variables(bool greedy) {
  _h_m.clear();
  _h_m.resize(noOfCoefficients);

  // create h_m:
  for (int m{0}; m < _h_m.size(); ++m) {
    stringstream strstrh;
    strstrh << "h_" << m;
    _h_m[m] = ScaLP::newIntegerVariable(strstrh.str());
  }

  // create h_m_is_zero:
  _h_m_is_zero.clear();
  _h_m_is_zero.resize(noOfCoefficients);
  // create _h_m_is_zero:
  for (int m{0}; m < _h_m_is_zero.size(); ++m) {
    stringstream strstrhmw;
    strstrhmw << "h_" << m << "_is_zero";
    _h_m_is_zero[m] = ScaLP::newBinaryVariable(strstrhmw.str());
  }

  // create gain variable
  if (greedy)
    cout << "creating variable gain (greedy solve)" << endl;
  else
    cout << "creating variable gain (solve)" << endl;
  gain = ScaLP::newRealVariable("gain", gain_bounds.first, gain_bounds.second);

  ScaLP::Term obj(0);

  // add objective only when zero coefficients are possible at all
  if (_target_set.find(0) != _target_set.end()) {
    IF_VERBOSE(3) cout << "adding structural adder cost to objective" << endl;
    switch (ir_type) {
    case SYM_EVEN:
      obj = obj - _h_m_is_zero[0];
      for (int m = 1; m < noOfCoefficients; m++) {
        obj = obj - 2 * _h_m_is_zero[m];
      }
      break;
    case SYM_ODD:
      for (int m = 0; m < noOfCoefficients; m++) {
        obj = obj - 2 * _h_m_is_zero[m];
      }
      break;
    case ASYM_EVEN:
      for (int m = 1; m < noOfCoefficients; m++) {
        obj = obj - 2 * _h_m_is_zero[m];
      }
      break;
    case ASYM_ODD:
      for (int m = 0; m < noOfCoefficients; m++) {
        obj = obj - 2 * _h_m_is_zero[m];
      }
      break;
    }
  } else {
    IF_VERBOSE(1)
    cout << "skipping adding of structural adder cost to "
         << "objective as no zero coefficient present" << endl;
  }

  _solver.setObjective(ScaLP::minimize(obj));
}

void FIROpt_Sparse::initialize_constraints() {
  if (!useFixedGain) {
    _solver << (gain >= gain_bounds.first);
    _solver << (gain <= gain_bounds.second);
  }

  int i{0};

  // constraint C1a:
  // the constraints on the filter's transfer function
  // these constraints are unified for each band.
  for (int band_i{0}; band_i < _no_bands; ++band_i) {
    for (const double &omega : _omega[band_i]) {
      ScaLP::Term term_c1a(0);

      double lower_bound =
          _bounds[band_i](omega, _A_bands_scaled[band_i].first);
      double upper_bound =
          _bounds[band_i](omega, _A_bands_scaled[band_i].second);

      for (int m{0}; m < noOfCoefficients; ++m)
        term_c1a = term_c1a + _basis[m](omega) * _h_m[m];
      ++i;
      if (useFixedGain) {
        IF_VERBOSE(4)
        std::cout << "C1(#" << i << ") :" << lower_bound * gainInit << " <= " << term_c1a
                  << "<=" << upper_bound * gainInit << endl;
        _solver << (upper_bound * gainInit >= term_c1a);
        _solver << (lower_bound * gainInit <= term_c1a);
      } else {
        IF_VERBOSE(4)
        std::cout << "C1(#" << i << ") :" << lower_bound << "*" << gain
                  << " <= " << term_c1a << "<=" << upper_bound << "*" << gain
                  << endl;
        _solver << (0 <= term_c1a - lower_bound * gain);
        _solver << (term_c1a - upper_bound * gain <= 0);
      }
    }
  }

  // constraint C1b:
  IF_VERBOSE(3)
  cout << "Trying to determine the bounds on the impulse response..." << endl;
  for (int i{0}; i < _h_m_bounds.size(); ++i) {
    IF_VERBOSE(4)
    cout << "C1b(#" << i << "): " << _h_m_bounds[i].first << "<= h_" << i
         << " <=" << _h_m_bounds[i].second << endl;

    _solver << (_h_m[i] >= _h_m_bounds[i].first);
    _solver << (_h_m[i] <= _h_m_bounds[i].second);
  }

  // constraint C9:
  for (int m{0}; m < _h_m_is_zero.size(); ++m) {
    if (useIndicatorConstraints) {
      _solver << (_h_m_is_zero[m] == 1 then _h_m[m] == 0);
    } else {
      _solver << (_h_m[m] - bigM + bigM * _h_m_is_zero[m] <= 0);
      _solver << (-_h_m[m] - bigM + bigM * _h_m_is_zero[m] <= 0);
    }
  }
}

void FIROpt_Sparse::greedy_solve() {
  // prepare problem:
  prepare_problem();

  long targetCoeffMax = pow(2, wordlength);
  if (shiftMax <= 0) {
    // use one shift larger than coefficient word size
    shiftMax = ((int)ceil(log2(targetCoeffMax))) + 1;
  }
  if (coeffMax <= 0)
    coeffMax = 2 * targetCoeffMax;
  if (bigM <= 0) {
    // set bigM to the minimum possible
    bigM = (coeffMax + 1) * (1L << shiftMax);
  }

  IF_VERBOSE(1) std::cout << "Solve Sparse - greedy version." << std::endl;

  IF_VERBOSE(1) cout << "settings:" << endl;
  IF_VERBOSE(1) cout << "timeout=" << timeout << endl;
  IF_VERBOSE(1)
  cout << "use indicator constraints=" << useIndicatorConstraints << endl;
  IF_VERBOSE(1) cout << "threads=" << threads << endl;

  _solver.reset();
  // print the solver used
  IF_VERBOSE(1) cout << "solver = " << _solver.getBackendName() << endl;
  // disable solver output
  _solver.quiet = false;
  // enable presolving
  _solver.presolve = true;
  // set number of threads
  _solver.threads = threads;
  // set the timeout of the solver
  if (timeout > 0)
    _solver.timeout = timeout;

#if 0 // not supported by all backends
    _solver.intFeasTol = intFeasTol;
        // the minimum tolerance accepted by the model
        double minTol = 1 / ((double) 2 * bigM);
        if(_solver.intFeasTol > minTol)
            _solver.intFeasTol = minTol;

        IF_VERBOSE(1) cout << "Int feasibility tolerance set to "
                            << _solver.intFeasTol << endl;
#endif
  // initialize variables:
  initialize_variables(true);

  initialize_start_values();

  initialize_constraints();

  stringstream ss;
  ss << outputRootName << ".lp";
  _solver.writeLP(ss.str());
  i_h_m.resize(_h_m.size());

  double g, ig;
  ScaLP::Result res;
  do {
    if (greedyCalls == 0) {
      // try to solve
      _solver.quiet = false;
      _stat = _solver.solve(startValues);

      if (_stat == ScaLP::status::FEASIBLE or _stat == ScaLP::status::OPTIMAL or
          _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
        res = _solver.getResult();
        for (unsigned i{0u}; i < _h_m.size(); ++i)
          i_h_m[i] = round(res.values[_h_m[i]]);
        if (useFixedGain)
          g = 1.0;
        else
          g = res.values[gain];
      } else {
        break;
      }
      cout << "Intermediary gain = " << std::setprecision(20) << g
           << std::setprecision(7) << endl;
      cout << "Intermediate h = [";
      for (unsigned i{0u}; i < _h_m.size(); ++i) {
        cout << (int)round(res.values[_h_m[i]]);
        if (i < _h_m.size() - 1)
          cout << " ";
      }
      cout << "]" << endl;

      if (dirty_validate(g))
        return;
    }

    ++greedyCalls;
    ig = g;

    auto toAdd = max_errors(g);
    bool newPoints = false;
    for (auto &maxPoint : toAdd) {
      bool newPoint = true;
      for (const double &omega : _omega[maxPoint.first]) {
        if (maxPoint.second == omega)
          newPoint = false;
      }
      if (newPoint)
        _omega[maxPoint.first].push_back(maxPoint.second);
      newPoints = newPoints or newPoint;
    }

    if (!newPoints) {
      cout << "Numerical problem detected: trying to add only "
           << "points already present in the discretization!\n"
           << "Stopping the greedy search procedure...\n";
      return;
    }
    for (auto &maxPoint : toAdd) {
      ScaLP::Term term_c2(0);

      double lower_bound = _bounds[maxPoint.first](
          maxPoint.second, _A_bands_scaled[maxPoint.first].first);
      double upper_bound = _bounds[maxPoint.first](
          maxPoint.second, _A_bands_scaled[maxPoint.first].second);

      for (int m{0}; m < noOfCoefficients; ++m)
        term_c2 = term_c2 + _basis[m](maxPoint.second) * _h_m[m];

      if (useFixedGain)
        _solver << (lower_bound <= term_c2 <= upper_bound);
      else {
        _solver << (0 <= term_c2 - lower_bound * gain);
        _solver << (term_c2 - upper_bound * gain <= 0);
      }
    }

    // try to solve
    _solver.quiet = false;
    // solve again, initialize with last solution
    _stat = _solver.solve(res);

    if (_stat == ScaLP::status::FEASIBLE or _stat == ScaLP::status::OPTIMAL or
        _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
      ScaLP::Result res = _solver.getResult();
      for (unsigned i{0u}; i < _h_m.size(); i++)
        i_h_m[i] = round(res.values[_h_m[i]]);

      if (useFixedGain)
        g = 1.0;
      else
        g = res.values[gain];

      cout << "Intermediate gain = " << std::setprecision(100) << g
           << std::setprecision(7) << endl;
      cout << "Intermediate h = [ ";
      for (unsigned i{0u}; i < _h_m.size(); i++) {
        cout << (int)round(res.values[_h_m[i]]);
        if (i < _h_m.size() - 1)
          cout << " ";
      }
      cout << "]" << endl;

      if (dirty_validate(g))
        return;
    } else {
      break;
    }
  } while (!dirty_validate(g));
}

void FIROpt_Sparse::solve() {
  // prepare problem
  prepare_problem();

  long targetCoeffMax = pow(2, wordlength);
  if (shiftMax <= 0) {
    // use one shift larger than coefficient word size
    shiftMax = ((int)ceil(log2(targetCoeffMax))) + 1;
  }
  if (coeffMax <= 0)
    coeffMax = 2 * targetCoeffMax;
  if (bigM <= 0) {
    // set bigM to the minimum possible
    bigM = (coeffMax + 1) * (1L << shiftMax);
  }

  IF_VERBOSE(1) std::cout << "Solve Sparse." << std::endl;

  IF_VERBOSE(1) cout << "settings:" << endl;
  IF_VERBOSE(1) cout << "timeout=" << timeout << endl;
  IF_VERBOSE(1) cout << "threads=" << threads << endl;

  _solver.reset();

  // print the used Solver
  IF_VERBOSE(1) cout << "solver=" << _solver.getBackendName() << endl;
  // disable solver output
  _solver.quiet = false;
  // enable presolving
  _solver.presolve = true;
  // set number of threads
  _solver.threads = threads;
  // set the timeout of the solver
  if (timeout > 0)
    _solver.timeout = timeout;

#if 0 // not supported by all backends
    _solver.intFeasTol = intFeasTol;
        // the minimum tolerance accepted by the model
        double minTol = 1 / ((double) 2 * bigM);
        if (_solver.intFeasTol > minTol)
            _solver.intFeasTol = minTol;

        IF_VERBOSE(1) cout << "Int feasibility tolerance set to "
                            << _solver.intFeasTol << endl;
#endif

  // initialize variables:
  initialize_variables();

  initialize_start_values();

  initialize_constraints();

  stringstream ss;
  ss << outputRootName << ".lp";
  _solver.writeLP(ss.str());

  // try to solve
  _stat = _solver.solve(startValues);

  if (_stat == ScaLP::status::FEASIBLE or _stat == ScaLP::status::OPTIMAL or
      _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
    i_h_m.resize(_h_m.size());
    ScaLP::Result res = _solver.getResult();
    IF_VERBOSE(1) std::cout << res.showSolutionVector(false) << endl;
    for (unsigned i{0}; i < _h_m.size(); i++)
      i_h_m[i] = round(res.values[_h_m[i]]);
  }
}

int FIROpt_Sparse::printSolution() {

  // also print the problem and solution information
  // inside a MATLAB script file
  stringstream ss;
  ss << outputRootName << ".m";
  ofstream outputInfo(ss.str());

  int exitCode = 1;
  // print results
  IF_VERBOSE(1) cout << "Print sparse solution." << endl;

  ScaLP::Result res = _solver.getResult();
  IF_VERBOSE(1) cout << res.showSolutionVector(true) << endl;

  i_h_m.resize(_h_m.size());
  if (_stat == ScaLP::status::FEASIBLE or _stat == ScaLP::status::OPTIMAL or
      _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
    for (unsigned i = 0; i < _h_m.size(); i++)
      i_h_m[i] = round(res.values[_h_m[i]]);
  }

  int t;
  switch (ir_type) {
  case SYM_EVEN: {
    t = 1;
    break;
  }
  case SYM_ODD: {
    t = 2;
    break;
  }
  case ASYM_EVEN: {
    t = 3;
    break;
  }
  default: { // ASYM_ODD
    t = 4;
    break;
  }
  }
  outputInfo << "type=" << t << ";" << endl;

  outputInfo << "wordlength=" << wordlength << ";\n";
  outputInfo << "fbands={";
  for (int i{0}; i < F_bands.size() - 1; ++i)
    outputInfo << "[" << F_bands[i].first << "," << F_bands[i].second << "],";
  outputInfo << "[" << F_bands[F_bands.size() - 1].first << ","
             << F_bands[F_bands.size() - 1].second << "]};\n";

  outputInfo << "abands={";
  for (int i{0}; i < A_bands.size() - 1; ++i)
    outputInfo << "[" << A_bands[i].first << "," << A_bands[i].second << "],";
  outputInfo << "[" << A_bands[A_bands.size() - 1].first << ","
             << A_bands[A_bands.size() - 1].second << "]};\n";

  //      std::setprecision(20);

  cout << "Optimization result: " << _stat << endl;
  if ((_stat == ScaLP::status::FEASIBLE) || (_stat == ScaLP::status::OPTIMAL) ||
      (_stat == ScaLP::status::TIMEOUT_FEASIBLE)) {
    outputInfo << "h=[";
    for (unsigned i = 0; i < _h_m.size(); i++) {
      outputInfo << round(i_h_m[i]);
      if (i < _h_m.size() - 1)
        outputInfo << " ";
    }
    outputInfo << "];" << endl;

    double g;
    if (useFixedGain) {
      g = 1.0;
    } else {
      g = res.values[gain];
    }
    outputInfo << "gain=" << g << ";" << endl;

    int filter_order = getFilterOrder();
    cout << "filter_order=" << filter_order << endl;
    cout << "objective=" << _solver.getResult().objectiveValue << endl;

    cout << "h=[";
    for (unsigned int i{0u}; i < noOfCoefficients; i++) {
      cout << round(res.values[_h_m[i]]);
      if (i < noOfCoefficients - 1)
        cout << " ";
    }
    cout << "]" << endl;

    cout << "Started naive validation...\n";
    if (dirty_validate(g)) {
      cout << "naive_validation=OK\n";
      exitCode = 0;
    } else {
      cout << "naive_validation=FAILED\n";
      exitCode = 10;
    }
    exitCode = 0;
  } else {
    if (_stat == ScaLP::status::INFEASIBLE_OR_UNBOUND or
        _stat == ScaLP::status::INFEASIBLE or _stat == ScaLP::status::UNBOUND)
      exitCode = 11;
    else if (_stat == ScaLP::status::INVALID)
      exitCode = 12;
    else if (_stat == ScaLP::status::TIMEOUT_INFEASIBLE)
      exitCode = 13;
    else if (_stat == ScaLP::status::ERROR)
      exitCode = 14;
    else
      exitCode = 15;
  }

  outputInfo.close();

  return exitCode;
}

pair<bool, int> FIROpt_Sparse::getOptimalityStatus() {
  bool valid{false};
  int objVal{0};
  if (_stat == ScaLP::status::OPTIMAL) {
    double g;

    if (useFixedGain)
      g = fabs(gainInit);
    else
      g = _solver.getResult().values[gain];

    valid = dirty_validate(g);
    if (valid)
      objVal = round(_solver.getResult().objectiveValue);
  }
  return make_pair(valid, objVal);
}