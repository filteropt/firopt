//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#pragma once

#include "FIROpt.h"

class FIROpt_ILP_2 : public FIROpt {
public:
  // constructor
  FIROpt_ILP_2(std::list<std::string> solverWishList, int threads = 0,
               int timeout = -1, int noOfCoefficients = 3,
               IRtype ir_type = SYM_EVEN,
               std::string outputRootName = "ILP2_model",
               double_pair_vec_t Fbands = {std::make_pair(0.0, 0.3),
                                           std::make_pair(0.5, 1.0)},
               double_pair_vec_t Abands = {std::make_pair(11, 20),
                                           std::make_pair(-7, 7)},
               double_pair_t gain_bounds = std::make_pair(0.66, 1.34),
               int wordlength = 3, int grid_points = 10);

  void build_problem();
  void solve() override;
  void greedy_solve() override;
  int printSolution() override;
  void compute_complete_pipeline_set(int_set_t *target_set);
  void compute_pair_map(bool filter_pipeline_set = true);
  void compute_triplet_map(bool filter_pipeline_set = true);
  void print_pair_map();
  void print_triplet_map();
  void prepare_problem_AT();

protected:
  virtual void initialize_start_values() override;
  ScaLP::Term constraints_coefficients(int sign, int m);

  std::vector<ScaLP::Variable> _sign_m;
  std::vector<map<int_t, ScaLP::Variable>> _h_m_w;

private:
  // containers for ScaLP variables:
  int_variable_map_vec_t _adder_variable_map_vec;
  int_variable_map_vec_t _register_variable_map_vec;
  int_pair_variable_map_vec_t _uv_pair_variable_map_vec;

  int_triplet_variable_map_vec_t _tuv_triplet_variable_map_vec; //just for the ternary case

};
