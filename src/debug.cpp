//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

//    This file is adapted from the PAGSuite project, a suite of optimization
//    tools to optimize pipelined adder graphs. It is developed at the
//    University of Kassel and maintained by Martin Kumm
//    (martin.kumm@informatik.hs-fulda.de). You can use and/or modify it for
//    research purposes, for commercial use, please ask for a license. For more
//    information please visit http://www.uni-kassel.de/go/pagsuite.

#include <iostream>

using namespace std;

#include "debug.h"
#include "types.h"

int global_verbose = 1;

ostream &operator<<(ostream &s, sd_t &sd) {
  sd_t::iterator iter;
  for (iter = sd.begin(); iter != sd.end();) {
    if (*iter >= 0)
      s << " " << *iter;
    else
      s << *iter;

    ++iter;
    if (iter != sd.end())
      s << " ";
  }
  return s;
}

ostream &operator<<(ostream &s, sd_set_t &msd_set) {
  sd_set_t::iterator iter;
  for (iter = msd_set.begin(); iter != msd_set.end(); ++iter) {
    sd_t sd = *iter;
    s << "(" << sd << ")" << endl;
  }
  return s;
}
