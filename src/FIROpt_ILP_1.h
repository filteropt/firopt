//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#pragma once

#include "FIROpt.h"

class FIROpt_ILP_1 : public FIROpt {
public:
  // constructor:
  FIROpt_ILP_1(std::list<std::string> solverWishList, int threads = 0,
               int timeout = -1, int noOfCoefficients = 3,
               IRtype ir_type = SYM_EVEN,
               std::string outputRootName = "ILP1_model",
               double_pair_vec_t Fbands = {std::make_pair(0.0, 0.3),
                                           std::make_pair(0.5, 1.0)},
               double_pair_vec_t Abands = {std::make_pair(11, 20),
                                           std::make_pair(-7, 7)},
               double_pair_t gain_bounds = std::make_pair(0.66, 1.34),
               int wordlength = 3, int grid_points = 10, int shiftMax = -1,
               int coeffMax = -1, long bigM = -1, double intFeasTol = 1e-5,
               bool useIndicatorConstraints = false, bool useRelaxation = false,
               int noOfAddersMin = -1, int noOfAddersMax = -1,
               std::string ilp1Objective = "none");

  void solve() override;
  void greedy_solve() override;
  int printSolution() override;
  void parseSolution();

  // getters:
  int getShiftMax() { return shiftMax; };
  int getCoeffMax() { return coeffMax; };
  long getBigM() { return bigM; };
  double getIntFeasTol() { return intFeasTol; };
  bool getUseIndicatorConstraints() { return useIndicatorConstraints; };
  int getNoOfAddersMin() { return noOfAddersMin; };
  int getNoOffAddersMax() { return noOfAddersMax; };
  std::string getObjective() { return ilp1Objective; };

  // setters:
  void setShiftMax(int shiftMax) { this->shiftMax = shiftMax; };
  void setCoeffMax(int coeffMax) { this->coeffMax = coeffMax; };
  void setBigM(long bigM) { this->bigM = bigM; };
  void setIntFeasTol(double intFeasTol) { this->intFeasTol = intFeasTol; };
  void setUseIndicatorConstraints(bool useIndicatorConstraints) {
    this->useIndicatorConstraints = useIndicatorConstraints;
  };
  void setNoOfAddersMin(int noOfAddersMin) {
    this->noOfAddersMin = noOfAddersMin;
  };
  void setNoOffAddersMax(int noOfAddersMax) {
    this->noOfAddersMax = noOfAddersMax;
  };
  void setILP1Objective(std::string objective) {
    this->ilp1Objective = objective;
  };

private:
  void initialize_variables(std::vector<ScaLP::Variable> &gpc,
                            std::vector<ScaLP::Variable> &gpcLeft,
                            std::vector<ScaLP::Variable> &gpcRight,
                            std::vector<ScaLP::Variable> &gpcMiddle,
                            bool greedy = false);
  void initialize_constraints(std::vector<ScaLP::Variable> &gpc,
                              std::vector<ScaLP::Variable> &gpcLeft,
                              std::vector<ScaLP::Variable> &gpcRight,
                              std::vector<ScaLP::Variable> &gpcMiddle);

  int noOfAddersMin{-1};
  int noOfAddersMax{-1};
  int shiftMax{-1};
  int shiftMin{0};
  long coeffMax{-1};
  long bigMGPC{1000};                // should be enough
  std::string ilp1Objective{"none"}; // default objective for ILP1
  double intFeasTol{1e-5};

  std::vector<std::vector<std::vector<std::vector<ScaLP::Variable>>>>
      _o_a_j_s_p; // _o_a_j_s[a][m][s][phi]

  int noOfMultiplierBlockAdders;

  // define variables:
  std::vector<ScaLP::Variable> coeff;
  std::vector<ScaLP::Variable> coeffLeft;
  std::vector<ScaLP::Variable> coeffRight;
  std::vector<ScaLP::Variable> coeffShiftedLeft;
  std::vector<ScaLP::Variable> coeffShiftedRight;
  std::vector<ScaLP::Variable> coeffShiftedSignLeft;
  std::vector<ScaLP::Variable> coeffShiftedSignRight;
  std::vector<std::vector<ScaLP::Variable>> coeffIskLeft;
  std::vector<std::vector<ScaLP::Variable>> coeffIskRight;
  std::vector<std::vector<ScaLP::Variable>> shiftIsSLeft;
  std::vector<std::vector<ScaLP::Variable>> shiftIsSRight;
  std::vector<ScaLP::Variable> signLeft;
  std::vector<ScaLP::Variable> signRight;

  std::vector<ScaLP::Variable>
      _h_m_is_zero; // for structural adder optimization

  // the 'middle' input is used for ternary adders:
  std::vector<ScaLP::Variable> coeffMiddle;
  std::vector<ScaLP::Variable> coeffShiftedMiddle;
  std::vector<ScaLP::Variable> coeffShiftedSignMiddle;
  std::vector<std::vector<ScaLP::Variable>> coeffIskMiddle;
  std::vector<vector<ScaLP::Variable>> shiftIsSMiddle;
  std::vector<ScaLP::Variable> signMiddle;

  // 'state' variables for the running optimal candidate
  double sGain;
  int sObjVal;
  std::vector<int> sH;
  ScaLP::status sStat;
};
