//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "FIROpt_ILP_1.h"
#include "FIROpt_Sparse.h"

#include <ScaLP/Exception.h>
#include <ScaLP/Solver.h>
#include <ScaLP/SolverDynamic.h>
#include <fstream>

using std::cout;
using std::endl;
using std::list;
using std::string;
using std::stringstream;
using std::vector;

FIROpt_ILP_1::FIROpt_ILP_1(list<string> solverWishList, int threads,
                           int timeout, int noOfCoefficients, IRtype ir_type,
                           string outputRootName, double_pair_vec_t Fbands,
                           double_pair_vec_t Abands, double_pair_t gain_bounds,
                           int wordlength, int grid_points, int shiftMax,
                           int coeffMax, long bigM, double intFeasTol,
                           bool useIndicatorConstraints, bool useRelaxation,
                           int noOfAddersMin, int noOfAddersMax,
                           string ilp1Objective)

    : FIROpt(solverWishList, threads, timeout, noOfCoefficients, ir_type,
             outputRootName, Fbands, Abands, wordlength, grid_points,
             gain_bounds)

{
  this->shiftMax = shiftMax;
  this->coeffMax = coeffMax;
  this->bigM = bigM;
  this->intFeasTol = intFeasTol;
  this->useIndicatorConstraints = useIndicatorConstraints;
  this->useRelaxation = useRelaxation;
  this->noOfAddersMin = noOfAddersMin;
  this->noOfAddersMax = noOfAddersMax;
  this->ilp1Objective = ilp1Objective;
}

void FIROpt_ILP_1::initialize_variables(vector<ScaLP::Variable> &gpc,
                                        vector<ScaLP::Variable> &gpcLeft,
                                        vector<ScaLP::Variable> &gpcRight,
                                        vector<ScaLP::Variable> &gpcMiddle,
                                        bool greedy) {
  coeff.clear();
  coeff.resize(noOfMultiplierBlockAdders + 1);

  coeffLeft.clear();
  coeffLeft.resize(noOfMultiplierBlockAdders + 1);

  coeffRight.clear();
  coeffRight.resize(noOfMultiplierBlockAdders + 1);

  coeffShiftedLeft.clear();
  coeffShiftedLeft.resize(noOfMultiplierBlockAdders + 1);

  coeffShiftedRight.clear();
  coeffShiftedRight.resize(noOfMultiplierBlockAdders + 1);

  coeffShiftedSignLeft.clear();
  coeffShiftedSignLeft.resize(noOfMultiplierBlockAdders + 1);

  coeffShiftedSignRight.clear();
  coeffShiftedSignRight.resize(noOfMultiplierBlockAdders + 1);

  coeffIskLeft.clear();
  coeffIskLeft.resize(noOfMultiplierBlockAdders + 1,
                      vector<ScaLP::Variable>(noOfMultiplierBlockAdders + 1));

  coeffIskRight.clear();
  coeffIskRight.resize(noOfMultiplierBlockAdders + 1,
                       vector<ScaLP::Variable>(noOfMultiplierBlockAdders + 1));

  shiftIsSLeft.clear();
  shiftIsSLeft.resize(noOfMultiplierBlockAdders + 1,
                      vector<ScaLP::Variable>(shiftMax));

  shiftIsSRight.clear();
  shiftIsSRight.resize(noOfMultiplierBlockAdders + 1,
                       vector<ScaLP::Variable>(shiftMax));

  signLeft.clear();
  signLeft.resize(noOfMultiplierBlockAdders + 1);

  signRight.clear();
  signRight.resize(noOfMultiplierBlockAdders + 1);

  // the 'middle' input is used for ternary adders
  coeffMiddle.clear();
  coeffMiddle.resize(noOfMultiplierBlockAdders + 1);

  coeffShiftedMiddle.clear();
  coeffShiftedMiddle.resize(noOfMultiplierBlockAdders + 1);

  coeffShiftedSignMiddle.clear();
  coeffShiftedSignMiddle.resize(noOfMultiplierBlockAdders + 1);

  coeffIskMiddle.clear();
  coeffIskMiddle.resize(noOfMultiplierBlockAdders + 1,
                        vector<ScaLP::Variable>(noOfMultiplierBlockAdders + 1));

  shiftIsSMiddle.clear();
  shiftIsSMiddle.resize(noOfMultiplierBlockAdders + 1,
                        vector<ScaLP::Variable>(shiftMax));

  signMiddle.clear();
  signMiddle.resize(noOfMultiplierBlockAdders + 1);

  for (unsigned int a{0u}; a <= noOfMultiplierBlockAdders; ++a) {
    // could be also real but then it gets numerically unstable
    // (at least with gurobi)
    coeff[a] = ScaLP::newIntegerVariable("c" + to_string(a), 0, coeffMax);

    if (useRelaxation) {
      coeffLeft[a] =
          ScaLP::newRealVariable("c" + to_string(a) + "_in_l", 0, coeffMax);
      coeffRight[a] =
          ScaLP::newRealVariable("c" + to_string(a) + "_in_r", 0, coeffMax);
      coeffShiftedLeft[a] =
          ScaLP::newRealVariable("c" + to_string(a) + "_in_sh_l", 0, coeffMax);
      coeffShiftedRight[a] =
          ScaLP::newRealVariable("c" + to_string(a) + "_in_sh_r", 0, coeffMax);
      coeffShiftedSignLeft[a] = ScaLP::newRealVariable(
          "c" + to_string(a) + "_in_sh_sg_l", -coeffMax, coeffMax);
      coeffShiftedSignRight[a] = ScaLP::newRealVariable(
          "c" + to_string(a) + "_in_sh_sg_r", -coeffMax, coeffMax);
      if (useTernaryAdder) {
        coeffMiddle[a] =
            ScaLP::newRealVariable("c" + to_string(a) + "_in_m", 0, coeffMax);
        coeffShiftedMiddle[a] = ScaLP::newRealVariable(
            "c" + to_string(a) + "_in_sh_m", 0, coeffMax);
        coeffShiftedSignMiddle[a] = ScaLP::newRealVariable(
            "c" + to_string(a) + "_in_sh_sg_m", -coeffMax, coeffMax);
      }
    } else {
      coeffLeft[a] =
          ScaLP::newIntegerVariable("c" + to_string(a) + "_in_l", 0, coeffMax);
      coeffRight[a] =
          ScaLP::newIntegerVariable("c" + to_string(a) + "_in_r", 0, coeffMax);
      coeffShiftedLeft[a] = ScaLP::newIntegerVariable(
          "c" + to_string(a) + "_in_sh_l", 0, coeffMax);
      coeffShiftedRight[a] = ScaLP::newIntegerVariable(
          "c" + to_string(a) + "_in_sh_r", 0, coeffMax);
      coeffShiftedSignLeft[a] = ScaLP::newIntegerVariable(
          "c" + to_string(a) + "_in_sh_sg_l", -coeffMax, coeffMax);
      coeffShiftedSignRight[a] = ScaLP::newIntegerVariable(
          "c" + to_string(a) + "_in_sh_sg_r", -coeffMax, coeffMax);
      if (useTernaryAdder) {
        coeffMiddle[a] = ScaLP::newIntegerVariable("c" + to_string(a) + "_in_m",
                                                   0, coeffMax);
        coeffShiftedMiddle[a] = ScaLP::newIntegerVariable(
            "c" + to_string(a) + "_in_sh_m", 0, coeffMax);
        coeffShiftedSignMiddle[a] = ScaLP::newIntegerVariable(
            "c" + to_string(a) + "_in_sh_sg_m", -coeffMax, coeffMax);
      }
    }
    signLeft[a] = ScaLP::newBinaryVariable("sign" + to_string(a) + "l");
    signRight[a] = ScaLP::newBinaryVariable("sign" + to_string(a) + "r");
    if (useTernaryAdder) {
      signMiddle[a] = ScaLP::newBinaryVariable("sign" + to_string(a) + "m");
    }
    for (unsigned int k{0u}; k < noOfMultiplierBlockAdders; k++) {
      coeffIskLeft[a][k] = ScaLP::newBinaryVariable("c" + to_string(a) +
                                                    "_in_l_is_" + to_string(k));
      coeffIskRight[a][k] = ScaLP::newBinaryVariable(
          "c" + to_string(a) + "_in_r_is_" + to_string(k));
      if (useTernaryAdder) {
        coeffIskMiddle[a][k] = ScaLP::newBinaryVariable(
            "c" + to_string(a) + "_in_m_is_" + to_string(k));
      }
    }
    for (unsigned int s{0u}; s < shiftMax; s++) {
      shiftIsSLeft[a][s] = ScaLP::newBinaryVariable("shift" + to_string(a) +
                                                    "_in_l_is_" + to_string(s));
      shiftIsSRight[a][s] = ScaLP::newBinaryVariable(
          "shift" + to_string(a) + "_in_r_is_" + to_string(s));
      if (useTernaryAdder) {
        shiftIsSMiddle[a][s] = ScaLP::newBinaryVariable(
            "shift" + to_string(a) + "_in_m_is_" + to_string(s));
      }
    }
  }

  _h_m.clear();
  _h_m.resize(noOfCoefficients);

  // create h_m:
  for (int m{0}; m < _h_m.size(); ++m) {
    stringstream strstrh;
    strstrh << "h_" << m;
    _h_m[m] = ScaLP::newRealVariable(strstrh.str());
  }

  // create h_m_is_zero:
  _h_m_is_zero.clear();
  if ((objective == FIROpt::TOTAL_ADDERS) ||
      (objective == FIROpt::STRUCT_ADDERS)) {
    _h_m_is_zero.resize(noOfCoefficients);
    // create _h_m_is_zero:
    for (int m{0}; m < _h_m_is_zero.size(); ++m) {
      stringstream strstrhmw;
      strstrhmw << "h_" << m << "_is_zero";
      _h_m_is_zero[m] = ScaLP::newBinaryVariable(strstrhmw.str());
    }
  }

  _o_a_j_s_p.clear();
  _o_a_j_s_p.resize(
      noOfMultiplierBlockAdders + 1,
      vector<vector<vector<ScaLP::Variable>>>(
          noOfCoefficients, vector<vector<ScaLP::Variable>>(
                                shiftMax, vector<ScaLP::Variable>(2))));

  // create _o_a_j_s_p:
  for (unsigned int a{0u}; a <= noOfMultiplierBlockAdders; ++a) {
    for (int m{0}; m < noOfCoefficients; ++m) {
      for (unsigned int s{0u}; s < shiftMax; s++) {
        stringstream strstroajp;
        strstroajp << "o_" << a << "_" << m << "_" << s << "_positive";
        _o_a_j_s_p[a][m][s][0] = ScaLP::newBinaryVariable(strstroajp.str());
        stringstream strstroajn;
        strstroajn << "o_" << a << "_" << m << "_" << s << "_negative";
        _o_a_j_s_p[a][m][s][1] = ScaLP::newBinaryVariable(strstroajn.str());
      }
    }
  }

  // create gain variable
  if (greedy)
    cout << "creating variable gain (greedy solve)" << endl;
  else
    cout << "creating variable gain (solve)" << endl;
  gain = ScaLP::newRealVariable("gain", gain_bounds.first, gain_bounds.second);

  // this corresponds to ilp1Objective == "none"
  ScaLP::Term obj(0);
  if (ilp1Objective == "none") {
    if ((objective == FIROpt::TOTAL_ADDERS) ||
        (objective == FIROpt::STRUCT_ADDERS)) {
      // add objective only when zero coefficients are possible at all
      if (_target_set.find(0) != _target_set.end()) {
        IF_VERBOSE(3)
        cout << "adding structural adder cost to objective" << endl;
        switch (ir_type) {
        case SYM_EVEN:
          obj = obj - _h_m_is_zero[0];
          for (int m = 1; m < noOfCoefficients; m++) {
            obj = obj - 2 * _h_m_is_zero[m];
          }
          break;
        case SYM_ODD:
          for (int m = 0; m < noOfCoefficients; m++) {
            obj = obj - 2 * _h_m_is_zero[m];
          }
          break;
        case ASYM_EVEN:
          for (int m = 1; m < noOfCoefficients; m++) {
            obj = obj - 2 * _h_m_is_zero[m];
          }
          break;
        case ASYM_ODD:
          for (int m = 0; m < noOfCoefficients; m++) {
            obj = obj - 2 * _h_m_is_zero[m];
          }
          break;
        }
      } else {
        IF_VERBOSE(1)
        cout << "skipping adding of structural adder cost to "
             << "objective as no zero coefficient present" << endl;
      }
    }
  } else if (ilp1Objective == "minAccSum") {
    if ((objective == FIROpt::TOTAL_ADDERS) ||
        (objective == FIROpt::STRUCT_ADDERS)) {
      cout << "Error: ilp1Objective " << ilp1Objective
           << " can not be combined with current main objective" << endl;
      exit(-1);
    }

    for (unsigned int a{1u}; a <= noOfMultiplierBlockAdders; a++)
      obj += coeff[a];
  } else if (ilp1Objective == "minGPC") {
    if ((objective == FIROpt::TOTAL_ADDERS) ||
        (objective == FIROpt::STRUCT_ADDERS)) {
      cout << "Error: ilp1Objective " << ilp1Objective
           << " can not be combined with current main objective" << endl;
      exit(-1);
    }

    for (unsigned int a{0u}; a <= noOfMultiplierBlockAdders; a++) {
      // initialize the GPC variables:
      // could be also real but then it gets numerically
      // unstable (at least with gurobi)
      gpc[a] = ScaLP::newIntegerVariable("gpc" + to_string(a));
      gpcLeft[a] = ScaLP::newRealVariable("gpc_l" + to_string(a));
      gpcRight[a] = ScaLP::newRealVariable("gpc_r" + to_string(a));
      if (useTernaryAdder)
        gpcMiddle[a] = ScaLP::newRealVariable("gpc_m" + to_string(a));
    }
    for (unsigned int a{1u}; a <= noOfMultiplierBlockAdders; a++)
      obj += gpc[a];
  } else {
    cout << "Error: ilp1Objective " << ilp1Objective << " unknown" << endl;
    exit(-1);
  }

  _solver.setObjective(ScaLP::minimize(obj));
}

void FIROpt_ILP_1::initialize_constraints(vector<ScaLP::Variable> &gpc,
                                          vector<ScaLP::Variable> &gpcLeft,
                                          vector<ScaLP::Variable> &gpcRight,
                                          vector<ScaLP::Variable> &gpcMiddle) {
  int i{0};

  // constraint C1a:
  // the constraints on the filter's transfer function
  // these constraints are unified for each band.
  for (int band_i{0}; band_i < _no_bands; ++band_i) {
    for (const double &omega : _omega[band_i]) {
      ScaLP::Term term_c1a(0);

      double lower_bound =
          _bounds[band_i](omega, _A_bands_scaled[band_i].first);
      double upper_bound =
          _bounds[band_i](omega, _A_bands_scaled[band_i].second);

      for (int m{0}; m < noOfCoefficients; ++m)
        term_c1a = term_c1a + _basis[m](omega) * _h_m[m];
      ++i;
      if (useFixedGain) {
        IF_VERBOSE(4)
        std::cout << "C1(#" << i << ") :" << lower_bound * gainInit
                  << " <= " << term_c1a << "<=" << upper_bound * gainInit
                  << endl;
        _solver << (upper_bound * gainInit >= term_c1a);
        _solver << (lower_bound * gainInit <= term_c1a);
      } else {
        IF_VERBOSE(4)
        std::cout << "C1(#" << i << ") :" << lower_bound << "*" << gain
                  << " <= " << term_c1a << "<=" << upper_bound << "*" << gain
                  << endl;
        _solver << (0 <= term_c1a - lower_bound * gain);
        _solver << (term_c1a - upper_bound * gain <= 0);
      }
    }
  }

  // constraint C1b:
  IF_VERBOSE(3)
  cout << "Trying to determine the bounds on the impulse response..." << endl;
  for (int i{0}; i < _h_m_bounds.size(); ++i) {
    IF_VERBOSE(4)
    cout << "C1b(#" << i << "): " << _h_m_bounds[i].first << "<= h_" << i
         << " <=" << _h_m_bounds[i].second << endl;

    _solver << (_h_m[i] >= _h_m_bounds[i].first);
    _solver << (_h_m[i] <= _h_m_bounds[i].second);
  }

  // constraint C2:
  if (!useFixedGain) {
    _solver << (gain >= gain_bounds.first);
    _solver << (gain <= gain_bounds.second);
  }

  // constraint C3a:
  for (unsigned int a{0u}; a <= noOfMultiplierBlockAdders; ++a) {
    for (int m{0}; m < noOfCoefficients; ++m) {
      for (unsigned int s{0u}; s < shiftMax; ++s) {
        if (useIndicatorConstraints) {
          // if _o_a_j == 1 then h'm = ca //TODO: Check if implementation is
          // correct
          _solver << (_o_a_j_s_p[a][m][s][0] ==
                      1 then _h_m[m] - (1LL << s) * coeff[a] == 0);
          _solver << (_o_a_j_s_p[a][m][s][1] ==
                      1 then _h_m[m] + (1LL << s) * coeff[a] == 0);
        } else {
          _solver << (_h_m[m] - (1LL << s) * coeff[a] - bigM +
                          bigM * _o_a_j_s_p[a][m][s][0] <=
                      0);
          _solver << (-_h_m[m] + (1LL << s) * coeff[a] - bigM +
                          bigM * _o_a_j_s_p[a][m][s][0] <=
                      0);
          _solver << (_h_m[m] + (1LL << s) * coeff[a] - bigM +
                          bigM * _o_a_j_s_p[a][m][s][1] <=
                      0);
          _solver << (-_h_m[m] - (1LL << s) * coeff[a] - bigM +
                          bigM * _o_a_j_s_p[a][m][s][1] <=
                      0);
        }
      }
    }
  }

  // optional constraints for structural adders:
  if ((objective == FIROpt::TOTAL_ADDERS) ||
      (objective == FIROpt::STRUCT_ADDERS)) {
    // constraint C3b:
    for (int m{0}; m < _h_m_is_zero.size(); ++m) {
      if (useIndicatorConstraints) {
        _solver << (_h_m_is_zero[m] == 1 then _h_m[m] == 0);
      } else {
        _solver << (_h_m[m] - bigM + bigM * _h_m_is_zero[m] <= 0);
        _solver << (-_h_m[m] - bigM + bigM * _h_m_is_zero[m] <= 0);
      }
    }
  }

  // constraint C3c:
  i = 0;
  for (int m{0}; m < noOfCoefficients; ++m) {
    ScaLP::Term term = 0;
    for (unsigned int a{0u}; a <= noOfMultiplierBlockAdders; a++)
      for (unsigned int s{0u}; s < shiftMax; s++)
        term = term + _o_a_j_s_p[a][m][s][0] + _o_a_j_s_p[a][m][s][1];

    if ((objective == FIROpt::TOTAL_ADDERS) ||
        (objective == FIROpt::STRUCT_ADDERS))
      term = term + _h_m_is_zero[m];

    IF_VERBOSE(4) std::cout << "C3c(#" << i << ")\t" << term << " == 1" << endl;
    i++;
    _solver << (term == 1);
  }

  // constraint C4:
  _solver << (coeff[0] == 1);
  for (unsigned int a{1u}; a <= noOfMultiplierBlockAdders; ++a) {
    // constraint C5:
    if (useTernaryAdder)
      _solver << (coeff[a] - coeffShiftedSignLeft[a] -
                      coeffShiftedSignMiddle[a] - coeffShiftedSignRight[a] ==
                  0);
    else
      _solver << (coeff[a] - coeffShiftedSignLeft[a] -
                      coeffShiftedSignRight[a] ==
                  0);

    for (unsigned int k{0u}; k <= a - 1; ++k) {
      // constraint C6a:
      if (useIndicatorConstraints) {
        _solver << (coeffIskLeft[a][k] == 1 then coeffLeft[a] - coeff[k] == 0);
        _solver << (coeffIskRight[a][k] == 1 then coeffRight[a] - coeff[k] ==
                    0);
        if (useTernaryAdder)
          _solver << (coeffIskMiddle[a][k] ==
                      1 then coeffMiddle[a] - coeff[k] == 0);
      } else {
        _solver << (coeff[k] - bigM + bigM * coeffIskLeft[a][k] -
                        coeffLeft[a] <=
                    0);
        _solver << (coeffLeft[a] - coeff[k] - bigM +
                        bigM * coeffIskLeft[a][k] <=
                    0);
        _solver << (coeff[k] - bigM + bigM * coeffIskRight[a][k] -
                        coeffRight[a] <=
                    0);
        _solver << (coeffRight[a] - coeff[k] - bigM +
                        bigM * coeffIskRight[a][k] <=
                    0);
        if (useTernaryAdder) {
          _solver << (coeff[k] - bigM + bigM * coeffIskMiddle[a][k] -
                          coeffMiddle[a] <=
                      0);
          _solver << (coeffMiddle[a] - coeff[k] - bigM +
                          bigM * coeffIskMiddle[a][k] <=
                      0);
        }
      }
    }

    // constraints C6b:
    ScaLP::Term c3bl(0), c3bm(0), c3br(0);
    for (unsigned int k{0}; k <= a - 1; ++k) {
      c3bl += coeffIskLeft[a][k];
      c3br += coeffIskRight[a][k];
      if (useTernaryAdder)
        c3bm += coeffIskMiddle[a][k];
    }
    _solver << (c3bl == 1);
    _solver << (c3br == 1);
    if (useTernaryAdder)
      _solver << (c3bm == 1);

    // constraint C7a:
    for (int s{shiftMin}; s < shiftMax; ++s) {
      if (useIndicatorConstraints) {
        _solver << (shiftIsSLeft[a][s] ==
                    1 then coeffShiftedLeft[a] - (1LL << s) * coeffLeft[a] ==
                    0);
        if (useTernaryAdder)
          _solver << (shiftIsSMiddle[a][s] ==
                      1 then coeffShiftedMiddle[a] -
                          (1LL << s) * coeffMiddle[a] ==
                      0);
      } else {
        _solver << (coeffShiftedLeft[a] - (1LL << s) * coeffLeft[a] - bigM +
                        bigM * shiftIsSLeft[a][s] <=
                    0);
        _solver << ((1LL << s) * coeffLeft[a] - bigM +
                        bigM * shiftIsSLeft[a][s] - coeffShiftedLeft[a] <=
                    0);
        if (useTernaryAdder) {
          _solver << (coeffShiftedMiddle[a] - (1LL << s) * coeffMiddle[a] -
                          bigM + bigM * shiftIsSMiddle[a][s] <=
                      0);
          _solver << ((1LL << s) * coeffMiddle[a] - bigM +
                          bigM * shiftIsSMiddle[a][s] - coeffShiftedMiddle[a] <=
                      0);
        }
      }
      // constraint C7c/d:
      if (s == 0)
        // shift at right input is zero for all positive shifts
        _solver << (shiftIsSRight[a][s] == 1);
      else if (s > 0)
        // shift at right input is zero for all positive shifts
        _solver << (shiftIsSRight[a][s] == 0);
      else
        // for right shifts, both have to be equal
        _solver << (shiftIsSRight[a][s] - shiftIsSLeft[a][s] == 0);
    }
    // right shift is currently disabled
    _solver << (coeffShiftedRight[a] - coeffRight[a] == 0);

    // constraints C7b:
    ScaLP::Term c7l(0), c7m(0), c7r(0);
    for (int s{0}; s < shiftMax; s++) {
      c7l += shiftIsSLeft[a][s];
      c7r += shiftIsSRight[a][s];
      if (useTernaryAdder)
        c7m += shiftIsSMiddle[a][s];
    }
    _solver << (c7l == 1);
    _solver << (c7r == 1);
    if (useTernaryAdder)
      _solver << (c7m == 1);

    // constraint C8a/b:
    if (useIndicatorConstraints) {
      // sign is +:
      _solver << (signLeft[a] ==
                  1 then coeffShiftedSignLeft[a] + coeffShiftedLeft[a] == 0);
      // sign is -:
      _solver << (signLeft[a] ==
                  0 then coeffShiftedSignLeft[a] - coeffShiftedLeft[a] == 0);
      // sign is +:
      _solver << (signRight[a] ==
                  1 then coeffShiftedSignRight[a] + coeffShiftedRight[a] == 0);
      // sign is -:
      _solver << (signRight[a] ==
                  0 then coeffShiftedSignRight[a] - coeffShiftedRight[a] == 0);
      if (useTernaryAdder) {
        // sign is +:
        _solver << (signMiddle[a] ==
                    1 then coeffShiftedSignMiddle[a] + coeffShiftedMiddle[a] ==
                    0);
        // sign is -:
        _solver << (signMiddle[a] ==
                    0 then coeffShiftedSignMiddle[a] - coeffShiftedMiddle[a] ==
                    0);
      }
    } else {
      // sign is +:
      _solver << (coeffShiftedSignLeft[a] - coeffShiftedLeft[a] -
                      bigM * signLeft[a] <=
                  0);
      _solver << (coeffShiftedLeft[a] - bigM * signLeft[a] -
                      coeffShiftedSignLeft[a] <=
                  0);
      // sign is -:
      _solver << (coeffShiftedSignLeft[a] + coeffShiftedLeft[a] - bigM +
                      bigM * signLeft[a] <=
                  0);
      _solver << (-coeffShiftedLeft[a] - bigM + bigM * signLeft[a] -
                      coeffShiftedSignLeft[a] <=
                  0);

      // sign is +:
      _solver << (coeffShiftedSignRight[a] - coeffShiftedRight[a] -
                      bigM * signRight[a] <=
                  0);
      _solver << (coeffShiftedRight[a] - bigM * signRight[a] -
                      coeffShiftedSignRight[a] <=
                  0);
      // sign is -:
      _solver << (coeffShiftedSignRight[a] + coeffShiftedRight[a] - bigM +
                      bigM * signRight[a] <=
                  0);
      _solver << (-coeffShiftedRight[a] - bigM + bigM * signRight[a] -
                      coeffShiftedSignRight[a] <=
                  0);

      if (useTernaryAdder) {
        // sign is +:
        _solver << (coeffShiftedSignMiddle[a] - coeffShiftedMiddle[a] -
                        bigM * signMiddle[a] <=
                    0);
        _solver << (coeffShiftedMiddle[a] - bigM * signMiddle[a] -
                        coeffShiftedSignMiddle[a] <=
                    0);
        // sign is -:
        _solver << (coeffShiftedSignMiddle[a] + coeffShiftedMiddle[a] - bigM +
                        bigM * signMiddle[a] <=
                    0);
        _solver << (-coeffShiftedMiddle[a] - bigM + bigM * signMiddle[a] -
                        coeffShiftedSignMiddle[a] <=
                    0);
      }
    }

    // constraint C8c:
    if (useTernaryAdder)
      _solver << (signLeft[a] + signMiddle[a] + signRight[a] <= 2);
    else
      _solver << (signLeft[a] + signRight[a] <= 1);
  }

  // optional GPC constraints:
  if (ilp1Objective == "minGPC") {
    // constraint C9a:
    _solver << (gpc[0] == 0);

    // constraint C9b:
    for (unsigned int a{1u}; a <= noOfMultiplierBlockAdders; a++) {
      for (unsigned int k{0u}; k <= a - 1; k++) {
        if (useIndicatorConstraints) {
          _solver << (coeffIskLeft[a][k] == 1 then gpcLeft[a] - gpc[k] == 0);
          _solver << (coeffIskRight[a][k] == 1 then gpcRight[a] - gpc[k] == 0);
          if (useTernaryAdder)
            _solver << (coeffIskMiddle[a][k] == 1 then gpcMiddle[a] - gpc[k] ==
                        0);
        } else {
          _solver << (gpc[k] - bigMGPC + bigMGPC * coeffIskLeft[a][k] -
                          gpcLeft[a] <=
                      0);
          _solver << (gpcLeft[a] - gpc[k] - bigMGPC +
                          bigMGPC * coeffIskLeft[a][k] <=
                      0);
          _solver << (gpc[k] - bigMGPC + bigMGPC * coeffIskRight[a][k] -
                          gpcRight[a] <=
                      0);
          _solver << (gpcRight[a] - gpc[k] - bigMGPC +
                          bigMGPC * coeffIskRight[a][k] <=
                      0);
          if (useTernaryAdder) {
            _solver << (gpc[k] - bigMGPC + bigMGPC * coeffIskMiddle[a][k] -
                            gpcMiddle[a] <=
                        0);
            _solver << (gpcMiddle[a] - gpc[k] - bigMGPC +
                            bigMGPC * coeffIskMiddle[a][k] <=
                        0);
          }
        }
      }

      // constraint C9c:
      if (useTernaryAdder)
        _solver << (gpc[a] - gpcLeft[a] - gpcMiddle[a] - gpcRight[a] == 1);
      else
        _solver << (gpc[a] - gpcLeft[a] - gpcRight[a] == 1);
    }
  }
}

void FIROpt_ILP_1::greedy_solve() {
  FIROpt_Sparse sparsefir = FIROpt_Sparse(
      this->solverWishList, this->threads, this->timeout,
      this->noOfCoefficients, this->ir_type, this->outputRootName,
      this->F_bands, this->A_bands, this->gain_bounds, this->wordlength,
      this->_no_init_grid_points, this->shiftMax, this->coeffMax, this->bigM,
      this->intFeasTol, this->useIndicatorConstraints);

  sparsefir.gainInit = gainInit;
  sparsefir.useFixedGain = useFixedGain;
  sparsefir.greedy_solve();
  auto sparseSol = sparsefir.getOptimalityStatus();

  if (sparseSol.first) {
    // prepare problem:
    prepare_problem();

    IF_VERBOSE(1) std::cout << "Solve ILP1 - greedy version." << std::endl;

    long targetCoeffMax = pow(2, wordlength);
    if (shiftMax <= 0) {
      // use one shift larger than coefficient word size
      shiftMax = ((int)ceil(log2(targetCoeffMax))) + 1;
    }
    if (coeffMax <= 0)
      coeffMax = 2 * targetCoeffMax;
    if (bigM <= 0) {
      // set bigM to the minimum possible
      bigM = (coeffMax + 1) * (1L << shiftMax);
    }
    if (noOfAddersMin < 0)
      noOfAddersMin = 0;
    if (noOfAddersMax < 0)
      noOfAddersMax = INT_MAX;

    IF_VERBOSE(1) cout << "settings:" << endl;
    IF_VERBOSE(1) cout << "ilp1Objective=" << ilp1Objective << endl;
    IF_VERBOSE(1) cout << "no of adders min=" << noOfAddersMin << endl;
    IF_VERBOSE(1) cout << "no of adders max=" << noOfAddersMax << endl;
    IF_VERBOSE(1) cout << "timeout=" << timeout << endl;
    IF_VERBOSE(1) cout << "max shift=" << shiftMax << endl;
    IF_VERBOSE(1) cout << "max coeff=" << coeffMax << endl;
    IF_VERBOSE(1) cout << "big M=" << bigM << endl;
    IF_VERBOSE(1)
    cout << "use indicator constraints=" << useIndicatorConstraints << endl;
    IF_VERBOSE(1) cout << "use relaxations=" << useRelaxation << endl;
    IF_VERBOSE(1) cout << "threads=" << threads << endl;

    bool sparseSolFound{false};
    bool initSolver{false};
    int gTotalAdderCount{0};
    int gNoOfMultiplierBlockAdders{0};
    int noOfTotalAdders{0};

    try {
      noOfMultiplierBlockAdders = noOfAddersMin;
      while (!sparseSolFound) {

        IF_VERBOSE(1)
        cout << "******************************************************"
             << endl;
        IF_VERBOSE(1)
        cout << "  starting optimization with " << noOfMultiplierBlockAdders
             << " adders" << endl;
        IF_VERBOSE(1)
        cout << "******************************************************"
             << endl;

        _solver.reset();

        // print the solver used
        IF_VERBOSE(1) cout << "solver = " << _solver.getBackendName() << endl;
        // disable solver output
        _solver.quiet = false;
        // enable presolving
        _solver.presolve = true;
        // set number of threads
        _solver.threads = threads;
        // set the timeout of the solver
        if (timeout > 0)
          _solver.timeout = timeout;

#if 0 // not supported by all backends
                _solver.intFeasTol = intFeasTol;
                // the minimum tolerance accepted by the model
                double minTol = 1 / ((double) 2 * bigM);
                if(_solver.intFeasTol > minTol)
                    _solver.intFeasTol = minTol;

                IF_VERBOSE(1) cout << "Int feasibility tolerance set to "
                                << _solver.intFeasTol << endl;
#endif

        // define variables for minGPC ilp1Objective:
        vector<ScaLP::Variable> gpc(noOfMultiplierBlockAdders + 1);
        vector<ScaLP::Variable> gpcLeft(noOfMultiplierBlockAdders + 1);
        vector<ScaLP::Variable> gpcRight(noOfMultiplierBlockAdders + 1);
        vector<ScaLP::Variable> gpcMiddle(noOfMultiplierBlockAdders + 1);

        // initialize variables:
        initialize_variables(gpc, gpcLeft, gpcRight, gpcMiddle, true);

        initialize_start_values();

        initialize_constraints(gpc, gpcLeft, gpcRight, gpcMiddle);

        stringstream ss;
        ss << outputRootName << ".lp";
        _solver.writeLP(ss.str());
        i_h_m.resize(_h_m.size());

        double g, ig;
        ScaLP::Result res;
        bool dirtyValidationPassed{false};
        do {
          if (greedyCalls == 0) {
            // try to solve
            _solver.quiet = false;
            printModelStatistics();
            _stat = _solver.solve(startValues);

            if (_stat == ScaLP::status::FEASIBLE or
                _stat == ScaLP::status::OPTIMAL or
                _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
              res = _solver.getResult();
              for (unsigned i{0u}; i < _h_m.size(); ++i)
                i_h_m[i] = round(res.values[_h_m[i]]);
              if (useFixedGain)
                g = 1.0;
              else
                g = res.values[gain];
            } else {
              break;
            }
            cout << "Intermediary gain = " << std::setprecision(20) << g
                 << std::setprecision(7) << endl;
            cout << "Intermediate h = [";
            for (unsigned i{0u}; i < _h_m.size(); ++i) {
              cout << (int)round(res.values[_h_m[i]]);
              if (i < _h_m.size() - 1)
                cout << " ";
            }
            cout << "]" << endl;

            if (dirty_validate(g)) {
              dirtyValidationPassed = true;
              goto out;
            }
          }

          ++greedyCalls;
          ig = g;

          auto toAdd = max_errors(g);
          bool newPoints = false;
          for (auto &maxPoint : toAdd) {
            bool newPoint = true;
            for (const double &omega : _omega[maxPoint.first]) {
              if (maxPoint.second == omega)
                newPoint = false;
            }
            if (newPoint)
              _omega[maxPoint.first].push_back(maxPoint.second);
            newPoints = newPoints or newPoint;
          }
          if (!newPoints) {
            cout << "Numerical problem detected: trying to add only "
                 << "points already present in the discretization!\n"
                 << "Stopping the greedy search procedure...\n";
            goto out;
          }
          for (auto &maxPoint : toAdd) {
            ScaLP::Term term_c2(0);

            double lower_bound = _bounds[maxPoint.first](
                maxPoint.second, _A_bands_scaled[maxPoint.first].first);
            double upper_bound = _bounds[maxPoint.first](
                maxPoint.second, _A_bands_scaled[maxPoint.first].second);

            for (int m{0}; m < noOfCoefficients; ++m)
              term_c2 = term_c2 + _basis[m](maxPoint.second) * _h_m[m];

            if (useFixedGain)
              _solver << (lower_bound <= term_c2 <= upper_bound);
            else {
              _solver << (0 <= term_c2 - lower_bound * gain);
              _solver << (term_c2 - upper_bound * gain <= 0);
            }
          }

          // try to solve
          _solver.quiet = false;
          // solve again, initialize with last solution
          printModelStatistics();
          _stat = _solver.solve(res);

          if (_stat == ScaLP::status::FEASIBLE or
              _stat == ScaLP::status::OPTIMAL or
              _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
            ScaLP::Result res = _solver.getResult();
            for (unsigned i{0u}; i < _h_m.size(); i++)
              i_h_m[i] = round(res.values[_h_m[i]]);

            if (useFixedGain)
              g = 1.0;
            else
              g = res.values[gain];

            cout << "Intermediate gain = " << std::setprecision(100) << g
                 << std::setprecision(7) << endl;
            cout << "Intermediate h = [ ";
            for (unsigned i{0u}; i < _h_m.size(); i++) {
              cout << (int)round(res.values[_h_m[i]]);
              if (i < _h_m.size() - 1)
                cout << " ";
            }
            cout << "]" << endl;

            if (dirty_validate(g)) {
              dirtyValidationPassed = true;
              goto out;
            }
          } else {
            goto out;
          }
        } while (!dirty_validate(g));

      out:
        if (dirtyValidationPassed) {

          int currObj = round(_solver.getResult().objectiveValue);
          noOfTotalAdders =
              noOfMultiplierBlockAdders + currObj + getFilterOrder();
          if (currObj == sparseSol.second)
            sparseSolFound = true;

          if (!initSolver) {
            initSolver = true;
            gTotalAdderCount = noOfTotalAdders;
            gNoOfMultiplierBlockAdders = noOfMultiplierBlockAdders;
            sStat = _stat;
            parseSolution();
          } else if (gTotalAdderCount > noOfTotalAdders) {
            gTotalAdderCount = noOfTotalAdders;
            gNoOfMultiplierBlockAdders = noOfMultiplierBlockAdders;
            sStat = _stat;
            parseSolution();
          }
        }

        if (!sparseSolFound)
          ++noOfMultiplierBlockAdders;
      }

    } catch (ScaLP::Exception &e) {
      cerr << "Error: " << e << endl;
    }

    // set as solution the best one found during
    // the iterative process
    noOfTotalAdders = gTotalAdderCount;
    noOfMultiplierBlockAdders = gNoOfMultiplierBlockAdders;

  } else {
    cerr << "Error: the sparse solver failed to give a solution" << endl;
  }
}

void FIROpt_ILP_1::solve() {
  FIROpt_Sparse sparsefir = FIROpt_Sparse(
      this->solverWishList, this->threads, this->timeout,
      this->noOfCoefficients, this->ir_type, this->outputRootName,
      this->F_bands, this->A_bands, this->gain_bounds, this->wordlength,
      this->_no_init_grid_points, this->shiftMax, this->coeffMax, this->bigM,
      this->intFeasTol, this->useIndicatorConstraints);

  sparsefir.gainInit = gainInit;
  sparsefir.useFixedGain = useFixedGain;
  sparsefir.solve();
  auto sparseSol = sparsefir.getOptimalityStatus();

  if (sparseSol.first) {
    // prepare problem
    prepare_problem();

    IF_VERBOSE(1) std::cout << "Solve ILP1." << std::endl;

    long targetCoeffMax = pow(2, wordlength);

    if (shiftMax <= 0) {
      // use one shift larger than coefficient word size
      shiftMax = ((int)ceil(log2(targetCoeffMax))) + 1;
    }
    if (coeffMax <= 0)
      coeffMax = 2 * targetCoeffMax;
    if (bigM <= 0) {
      // set big M to the minimum possible
      bigM = (coeffMax + 1) * (1LL << shiftMax);
    }
    if (noOfAddersMin < 0)
      noOfAddersMin = 0;
    if (noOfAddersMax < 0)
      noOfAddersMax = INT_MAX;

    IF_VERBOSE(1) cout << "settings:" << endl;
    IF_VERBOSE(1) cout << "ilp1Objective=" << ilp1Objective << endl;
    IF_VERBOSE(1) cout << "no of adders min=" << noOfAddersMin << endl;
    IF_VERBOSE(1) cout << "no of adders max=" << noOfAddersMax << endl;
    IF_VERBOSE(1) cout << "timeout=" << timeout << endl;
    IF_VERBOSE(1) cout << "max shift=" << shiftMax << endl;
    IF_VERBOSE(1) cout << "max coeff=" << coeffMax << endl;
    IF_VERBOSE(1) cout << "big M=" << bigM << endl;
    IF_VERBOSE(1)
    cout << "use indicator constraints=" << useIndicatorConstraints << endl;
    IF_VERBOSE(1) cout << "use relaxations=" << useRelaxation << endl;
    IF_VERBOSE(1) cout << "threads=" << threads << endl;

    bool sparseSolFound{false};
    bool initSolver{false};
    int gTotalAdderCount{0};
    int gNoOfMultiplierBlockAdders{0};
    int noOfTotalAdders{0};

    try {
      noOfMultiplierBlockAdders = noOfAddersMin;
      while (!sparseSolFound) {
        IF_VERBOSE(1)
        cout << "******************************************************"
             << endl;
        IF_VERBOSE(1)
        cout << "  starting optimization with " << noOfMultiplierBlockAdders
             << " adders" << endl;
        IF_VERBOSE(1)
        cout << "******************************************************"
             << endl;

        _solver.reset();

        // print the used Solver
        IF_VERBOSE(1) cout << "solver=" << _solver.getBackendName() << endl;
        // disable solver output
        _solver.quiet = false;
        // enable presolving
        _solver.presolve = true;
        // set number of threads
        _solver.threads = threads;
        // set the timeout of the solver
        if (timeout > 0)
          _solver.timeout = timeout;

#if 0 // not supported by all backends
                _solver.intFeasTol = intFeasTol;
                // the minimum tolerance accepted by the model
                double minTol = 1 / ((double) 2 * bigM);
                if (_solver.intFeasTol > minTol)
                    _solver.intFeasTol = minTol;

                IF_VERBOSE(1) cout << "Int feasibility tolerance set to "
                                << _solver.intFeasTol << endl;
#endif

        // define variables for minGPC ilp1Objective:
        vector<ScaLP::Variable> gpc(noOfMultiplierBlockAdders + 1);
        vector<ScaLP::Variable> gpcLeft(noOfMultiplierBlockAdders + 1);
        vector<ScaLP::Variable> gpcRight(noOfMultiplierBlockAdders + 1);
        vector<ScaLP::Variable> gpcMiddle(noOfMultiplierBlockAdders + 1);

        // initialize variables:
        initialize_variables(gpc, gpcLeft, gpcRight, gpcMiddle);

        initialize_start_values();

        initialize_constraints(gpc, gpcLeft, gpcRight, gpcMiddle);

        stringstream ss;
        ss << outputRootName << ".lp";
        _solver.writeLP(ss.str());

        printModelStatistics();

        // try to solve
        _stat = _solver.solve(startValues);

        // exit loop if solution is found
        if (_stat == ScaLP::status::FEASIBLE or
            _stat == ScaLP::status::OPTIMAL or
            _stat == ScaLP::status::TIMEOUT_FEASIBLE) {
          ScaLP::Result res = _solver.getResult();

          int currObj = round(_solver.getResult().objectiveValue);
          noOfTotalAdders =
              noOfMultiplierBlockAdders + currObj + getFilterOrder();
          if (currObj == sparseSol.second)
            sparseSolFound = true;

          if (!initSolver) {
            initSolver = true;
            gTotalAdderCount = noOfTotalAdders;
            gNoOfMultiplierBlockAdders = noOfMultiplierBlockAdders;
            sStat = _stat;
            parseSolution();
          } else if (gTotalAdderCount > noOfTotalAdders) {
            gTotalAdderCount = noOfTotalAdders;
            gNoOfMultiplierBlockAdders = noOfMultiplierBlockAdders;
            sStat = _stat;
            parseSolution();
          }
        }
        if (!sparseSolFound)
          ++noOfMultiplierBlockAdders;
      }
      // set as solution the best one found during
      // the iterative process
      noOfTotalAdders = gTotalAdderCount;
      noOfMultiplierBlockAdders = gNoOfMultiplierBlockAdders;
    } catch (ScaLP::Exception &e) {
      cerr << "Error: " << e << endl;
    }
  } else { // the sparse solver failed to give a solution
    cerr << "Error: the sparse solver failed to give a solution" << endl;
  }
}

void FIROpt_ILP_1::parseSolution() {
  i_h_m.resize(_h_m.size());
  ScaLP::Result res = _solver.getResult();
  sStat = _stat;
  sH.resize(_h_m.size());
  if (sStat == ScaLP::status::FEASIBLE or sStat == ScaLP::status::OPTIMAL or
      sStat == ScaLP::status::TIMEOUT_FEASIBLE) {
    for (unsigned i = 0; i < _h_m.size(); i++)
      sH[i] = round(res.values[_h_m[i]]);
  }

  if (useFixedGain)
    sGain = 1.0;
  else
    sGain = res.values[gain];

  sObjVal = round(res.objectiveValue);
}

int FIROpt_ILP_1::printSolution() {
  // TODO: print solution
  int exitCode = 1;

  int t;
  switch (ir_type) {
  case SYM_EVEN: {
    t = 1;
    break;
  }
  case SYM_ODD: {
    t = 2;
    break;
  }
  case ASYM_EVEN: {
    t = 3;
    break;
  }
  default: { // ASYM_ODD
    t = 4;
    break;
  }
  }

  cout << "Optimization result: " << sStat << endl;

  if ((sStat == ScaLP::status::FEASIBLE) || (sStat == ScaLP::status::OPTIMAL) ||
      (sStat == ScaLP::status::TIMEOUT_FEASIBLE)) {
    cout << "Final gain = " << std::setprecision(100) << sGain
         << std::setprecision(7) << endl;
    cout << "Final h = [ ";
    for (unsigned i{0u}; i < sH.size(); i++) {
      cout << sH[i];
      if (i < sH.size() - 1)
        cout << " ";
    }
    cout << "]" << endl;

    int filterOrder = getFilterOrder();
    int noOfTotalAdders = noOfMultiplierBlockAdders + sObjVal + filterOrder;
    cout << "type=" << t << endl;
    cout << "filter_order=" << filterOrder << endl;
    cout << "number_of_adders=" << noOfTotalAdders << endl;
    cout << "number_of_mult_adders=" << noOfMultiplierBlockAdders << endl;
    cout << "number_of_structural_adders="
         << noOfTotalAdders - noOfMultiplierBlockAdders << endl;

    cout << "greedyCalls=" << greedyCalls << endl;

    cout << "Started naive validation...\n";
    if (dirty_validate(sGain)) {
      cout << "naive_validation=OK\n";
      exitCode = 0;
    } else {
      cout << "naive_validation=FAILED\n";
      exitCode = 10;
    }
  } else {
    if (sStat == ScaLP::status::INFEASIBLE_OR_UNBOUND or
        sStat == ScaLP::status::INFEASIBLE or sStat == ScaLP::status::UNBOUND)
      exitCode = 11;
    else if (sStat == ScaLP::status::INVALID)
      exitCode = 12;
    else if (sStat == ScaLP::status::TIMEOUT_INFEASIBLE)
      exitCode = 13;
    else if (sStat == ScaLP::status::ERROR)
      exitCode = 14;
    else
      exitCode = 15;
  }

  return exitCode;
}