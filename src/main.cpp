//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "FIROpt.h"
#include "FIROpt_ILP_1.h"
#include "FIROpt_ILP_2.h"
#include "FIROpt_Sparse.h"
#include <fstream>
#include <iostream>

using std::cout;
using std::endl;

/**
 * Returns the value as string of a command line argument in syntax --key=value
 * @param argv the command line string
 * @param parameter the name of the parameter
 * @param value as string if parameter string was found
 * @return True if parameter string was found
 */
bool getCmdParameter(char *argv, const char *parameter, char *&value) {
  if (strstr(argv, parameter)) {
    value = argv + strlen(parameter);
    return true;
  } else {
    return false;
  }
}

void print_short_help() {
  cout << "usage: firopt [OPTIONS]" << endl;
  cout << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "General Options:" << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "Option                                          Meaning" << endl;
  cout << "--help                                          Prints this help"
       << endl;
  cout << "--verbose=[int]                                 Verbosity level, "
          "0:no output, 1: default output, 2..3 debug output (default: 1)"
       << endl;
  cout << "--wordlength=[int]                              Coefficient "
          "wordlength (default: 8 bit)"
       << endl;
  cout << "--noOfCoefficients=[int]                        Number of "
          "coefficients (default: 13)"
       << endl;
  cout << "--ftype=[1|2|3|4]                               Filter Type. "
          "1:SYM_EVEN, 2:SYM_ODD, 3:ASYM_EVEN, 4:ASYM_ODD (default: 1)"
       << endl;
  cout << "--gain=[[float],[float]]|[float]                Bounds on the gain "
          "value domain or use a fixed gain. Will be computed based on a "
          "projection approach if no parameter is given"
       << endl;
  cout << "--grid=[uniform|nonuniform]                     Selects the "
          "frequency grid between naive uniform or 'smart' nonuniform grid "
          "(default: nonuniform)"
       << endl;
  cout << "--gridPoints=[int]                              Sets the number of "
          "grid points (default: 40)"
       << endl;
  cout << "--solver=[Gurobi|CPLEX|SCIP|LPSolve]            ILP solver (if "
          "available in ScaLP library), also a comma separated wish list of "
          "solvers can be provided"
       << endl;
  cout << "--greedy=[false|true]                           Specifies if a "
          "greedy refinement of the domain discretization is to be used in "
          "case the solution is not valid (default: false)"
       << endl;
  cout << "--formulation=[1|2]                             ILP formulation "
          "(default: 2)"
       << endl;
  cout << "--timeout=[int]                                 ILP solver Timeout "
          "in seconds (default: -1 - no timeout)"
       << endl;
  cout << "--threads=[int]                                 Number of threads "
          "for the ILP solver (default: 0 - auto-detection)"
       << endl;
  cout << "--no-relaxations                                When specified, "
          "relaxations to real variables are not used (only integer or binary)"
       << endl;
  cout << "--output_name=[string]                          When specified, "
          "uses [string] as the root name for all tool output files"
       << endl;
  cout << "--objective=[A|SA|MA]                           Selects the "
          "objective to be the number of adders (A), structural adders only "
          "(SA) and multiplier block adders only (MA) (default: A)"
       << endl;
  cout << "--extra_grid=[float],[float]...                 Adds user specified "
          "points to the discretization"
       << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "Post-Optimization (to enhance partially solved (non-optimal) "
          "problems):"
       << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "--coeff_init=[int],[int]...                     Initializes the "
          "coefficients h[0], h[1], ..., the specified coefficients"
       << endl;
  cout << "Note: Option --adder_graph_init=[adder graph] is not available as "
          "firopt was not linked against PAGLib"
       << endl;
#ifdef HAVE_PAGLIB
  cout << "--adder_graph_init=[string]                     Initializes the "
          "adder graph to the one described by the string. See PAGSuite "
          "website for details of the format."
       << endl;
#endif
  cout << "-----------------------------------------------" << endl;
  cout << "Filter Specification:" << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "Option                                          Meaning" << endl;
  cout << "--fbands=[[float],[float]],[[float],[float]]... Explicit "
          "declaration of fbands"
       << endl;
  cout << "--abands=[[float],[float]],[[float],[float]]... Explicit "
          "declaration of abands"
       << endl;
  cout << "--desired_bands=[int],[int]...                  Autocompute abands "
          "using desired bands and delta"
       << endl;
  cout << "--delta=[[float],[float]],[[float],[float]]...  Explicit "
          "declaration of delta"
       << endl;
  cout << "--delta=[float],[float]...                      Alternative "
          "declacation of delta. + and - will be autocomputed"
       << endl;
  cout << "--delta=[float]                                 Alternative "
          "declaration of delta. Uniform delta for all abands. + and - will be "
          "autocomputed. "
       << endl;
  cout << "--indicator_constraints                         When specified, "
          "indicator constraints are used instead of big-M constraints"
       << endl;
  cout << "--big_M=[int]                                   Value of the big-M "
          "variable (default: max. coeff + 1)"
       << endl;
  cout << "--ternary_add                                   When specified, "
          "ternary adders are considered instead of 2-input adders"
       << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "ILP1 specific Options:" << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "Option                                          Meaning" << endl;
  cout << "--max_shift=[int]                               Maximum bit shift "
          "which is considered in the adder graph"
       << endl;
  cout << "--max_coeff=[int]                               Maximum coefficient "
          "that is expected in an adder node"
       << endl;
  cout << "--int_feas_tol=[float]                          Sets the integer "
          "feasible tolerance of the solver. Reduce this value for large "
          "constants if errors occur (default: min(1E-5,1/(2*big-M)))"
       << endl;
  cout << "--no_of_adders_min=[int]                        Minimum number of "
          "adders for which a solution is searched (default is the number of "
          "unique odd coefficients as lower bound)"
       << endl;
  cout << "--no_of_adders_max=[int]                        Maximum number of "
          "adders for which a solution is searched (default is infinity)"
       << endl;
  cout << "--ILP1objective=[none|minAccSum|minGPC]         Secondary objective "
          "(default: none), none: only the adders are minimized, minAccSum: "
          "the sum of coefficients is minimized leading to low word size "
          "solutions, minGPC: the glitch path cound (GPC) is minimized"
       << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "ILP2 specific Options:" << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "Option                                          Meaning" << endl;
  cout << "--stages=[int]                                  Number of pipeline "
          "stages (default: -1 - auto-detection from wordlength)"
       << endl;
  cout << "--cost_add=[int]                                Cost of an adder "
          "(default: 1)"
       << endl;
  cout << "--cost_reg=[int]                                Cost of a register "
          "(default: 0)"
       << endl;
  cout << endl;
}

int main(int argc, char *args[]) {
  /*
   * General Options Variables
   */

  std::string preferredSolver = "";
  int ilp_formulation = 2; // default ILP formulation
  int threads = 0;
  int timeout = -1; // default -1 means no timeout
  int wordlength = 8;
  bool useUniformFrequencyGrid = false;
  bool useRelaxation = true;
  bool useGreedy = false;
  IRtype ir_type = SYM_EVEN;

  /*
   * Filter Spec Variables
   */

  stringstream fbands_strstr;
  stringstream abands_strstr;
  stringstream desired_bands_strstr;
  stringstream delta_strstr;
  stringstream extra_strstr;
  stringstream gain_strstr;
  double_pair_vec_t Fbands;
  double_pair_vec_t Abands;
  double_vec_t desiredBands;
  double_pair_vec_t delta;
  double_vec_t deltaAlternative;
  double_pair_t gainBounds = make_pair(0.01, 100.0);

  // plausibility check variables
  bool useFbandsArg = false;
  bool useAbandsArg = false;
  bool useDesiredBandsArg = false;
  bool useDeltaArg = false;
  bool useExtraPoints = false;
  bool useCoeffInit = false;
  bool useGainBounds = false;

  /*
   * ILP 2 specific variables
   */

  int no_of_pipeline_stages = -1;
  int cost_reg = 0; // initially, we solve an MCM problem
  int cost_add = 1;

  // plausibility check variables
  bool useNoOfPipelineStagesArg = false;
  bool useCostRegArg = false;
  bool useCostAddArg = false;

  /*
   * ILP 1 specific variables
   */

  int shiftMax = -1;
  long coeffMax = -1;
  long bigM = -1;
  double intFeasTol = 1e-5;
  bool useIndicatorConstraints = false;
  bool useTernaryAdder = false;
  int noOfAddersMin = -1;
  int noOfAddersMax = -1;
  string objectiveStr =
      "A"; // default objective is minimizing the total number of adders
  string greedyStr = "false";
  string ILP1objective = "none";   // default objective for ILP1
  string rootOutputName = "model"; // default root name for the output files
  FIROpt::ObjectiveType objective;

  int noOfCoefficients = 13;
  int grid_points = 80;
  std::vector<double> extraGridPoints;

  // initialization variables:
  stringstream coeff_init_strstr;
  std::vector<int> coeffInit; // coefficient values for initialization (warm
                              // start), empty string vector means ignore
  double gainInit = -1;
  string adderGraphInit = ""; // adder graph for initialization (warm start),
                              // empty string means ignore

  // plausibility check variables
  bool useShiftMaxArg = false;
  bool useCoeffMaxArg = false;
  bool usebigMArg = false;
  bool useIntFeasTolArg = false;
  bool useUseIndicatorConstraintsArg = false;
  bool useUseTernaryAdderArg = false;
  bool useNoOfAddersMinArg = false;
  bool useNoOfAddersMaxArg = false;
  bool useObectiveArg = false;
  bool useCustomGain = false;

  global_verbose = 1;

  // parse command line
  for (int i = 1; i < argc; i++) {
    char *value;
    if (getCmdParameter(args[i], "--help", value)) {
      print_short_help();
      exit(-1);
    } else if (getCmdParameter(args[i], "--verbose=", value)) {
      global_verbose = atol(value);
    } else if (getCmdParameter(args[i], "--wordlength=", value)) {
      wordlength = atol(value);
      if (wordlength < 2) {
        cout << "Error: Wordlength must be at least 2 but is set to "
             << wordlength << "\n";
        print_short_help();
        exit(-1);
      }
    } else if (getCmdParameter(args[i], "--output_name=", value)) {
      rootOutputName = value;
    } else if (getCmdParameter(args[i], "--noOfCoefficients=", value)) {
      noOfCoefficients = atoi(value);
      if (noOfCoefficients < 2) {
        cout << "Error: Filter order must be at least 2 but "
             << "is set to " << noOfCoefficients << "\n";
        print_short_help();
        exit(-1);
      }
    /*} else if (getCmdParameter(args[i], "--gain=", value)) {
      if (string(value) == "fixed")
        useFixedGain = true;
      else if (string(value) == "flexible")
        useFixedGain = false;
      else {
        cout << "Error: Illegal option for parameter gain: " << value << endl;
        print_short_help();
        exit(-1);
      } */
    } else if (getCmdParameter(args[i], "--grid=", value)) {
      if (string(value) == "uniform")
        useUniformFrequencyGrid = true;
      else if (string(value) == "nonuniform")
        useUniformFrequencyGrid = false;
      else {
        cout << "Error: Illegal option for parameter grid: " << value << endl;
        print_short_help();
        exit(-1);
      }
    } else if (getCmdParameter(args[i], "--gridPoints=", value)) {
      grid_points = atoi(value);
    } else if (getCmdParameter(args[i], "--ftype=", value)) {
      int ftype = atoi(value);
      if (ftype >= 1 && ftype <= 4) {
        ir_type = static_cast<IRtype>(atoi(value) - 1);
      } else {
        cout << "Error: Illegal option for parameter ftype: " << value << endl;
        exit(-1);
      }
    } else if (getCmdParameter(args[i], "--solver=", value)) {
      preferredSolver = std::string(value);
    } else if (getCmdParameter(args[i], "--formulation=", value)) {
      ilp_formulation = atoi(value);
      if (ilp_formulation < 1 or ilp_formulation > 2) {
        cout << "Error: Illegal option for ILP formulation: " << ilp_formulation
             << endl;
        print_short_help();
        exit(-1);
      }
    } else if (getCmdParameter(args[i], "--timeout=", value)) {
      timeout = atol(value);
    } else if (getCmdParameter(args[i], "--threads=", value)) {
      threads = atol(value);
    } else if (getCmdParameter(args[i], "--no-relaxations", value)) {
      useRelaxation = false;
    } else if (getCmdParameter(args[i], "--fbands", value)) {
      useFbandsArg = true;
      fbands_strstr << value;
    } else if (getCmdParameter(args[i], "--abands", value)) {
      useAbandsArg = true;
      abands_strstr << value;
    } else if (getCmdParameter(args[i], "--desired_bands", value)) {
      useDesiredBandsArg = true;
      desired_bands_strstr << value;
    } else if (getCmdParameter(args[i], "--delta", value)) {
      useDeltaArg = true;
      delta_strstr << value;
    } else if (getCmdParameter(args[i], "--extra_grid=", value)) {
      useExtraPoints = true;
      extra_strstr << value;
    } else if (getCmdParameter(args[i], "--coeff_init=", value)) {
      useCoeffInit = true;
      coeff_init_strstr << value;
    } else if (getCmdParameter(args[i], "--gain", value)) {
      useCustomGain = true;
      gain_strstr << value;
    } else if (getCmdParameter(args[i], "--adder_graph_init=", value)) {
#ifdef HAVE_PAGLIB
      adderGraphInit = value;
#else
      cout << "Error: adder graph specified but firopt was"
           << " not linked against PAGLib" << endl;
      exit(-1);
#endif // HAVE_PAGLIB
    } else if (getCmdParameter(args[i], "--stages=", value)) {
      useNoOfPipelineStagesArg = true;
      no_of_pipeline_stages = atol(value);
    } else if (getCmdParameter(args[i], "--cost_add=", value)) {
      useCostAddArg = true;
      cost_add = atol(value);
    } else if (getCmdParameter(args[i], "--cost_reg=", value)) {
      useCostRegArg = true;
      cost_reg = atol(value);
    } else if (getCmdParameter(args[i], "--max_shift=", value)) {
      useShiftMaxArg = true;
      shiftMax = atol(value);
    } else if (getCmdParameter(args[i], "--max_coeff=", value)) {
      useCoeffMaxArg = true;
      coeffMax = atol(value);
    } else if (getCmdParameter(args[i], "--big_M=", value)) {
      usebigMArg = true;
      bigM = atol(value);
    } else if (getCmdParameter(args[i], "--int_feas_tol=", value)) {
      useIntFeasTolArg = true;
      intFeasTol = atof(value);
    } else if (getCmdParameter(args[i], "--indicator_constraints", value)) {
      useUseIndicatorConstraintsArg = true;
      useIndicatorConstraints = true;
    } else if (getCmdParameter(args[i], "--ternary_add", value)) {
      useUseTernaryAdderArg = true;
      useTernaryAdder = true;
    } else if (getCmdParameter(args[i], "--no_of_adders_min=", value)) {
      useNoOfAddersMinArg = true;
      noOfAddersMin = atol(value);
    } else if (getCmdParameter(args[i], "--no_of_adders_max=", value)) {
      useNoOfAddersMaxArg = true;
      noOfAddersMax = atol(value);
    } else if (getCmdParameter(args[i], "--objective=", value)) {
      objectiveStr = value;
    } else if (getCmdParameter(args[i], "--ILP1objective=", value)) {
      useObectiveArg = true;
      ILP1objective = value;
    } else if (getCmdParameter(args[i], "--greedy=", value)) {
      greedyStr = value;
    } else {
      cout << "Error: Illegal option: " << args[i] << endl;
      print_short_help();
      exit(-1);
    }
  }

  // set objective
  if (objectiveStr == "A") {
    objective = FIROpt::TOTAL_ADDERS;
  } else if (objectiveStr == "SA") {
    objective = FIROpt::STRUCT_ADDERS;
  } else if (objectiveStr == "MA") {
    objective = FIROpt::MULT_ADDERS;
  } else {
    cout << "Error: Illegal option for objective: " << objectiveStr << endl;
    exit(-1);
  }

  // set normal or greedy approach
  if (greedyStr == "true")
    useGreedy = true;
  else if (greedyStr == "false")
    useGreedy = false;
  else {
    cout << "Error: Illegal option for greedy approach: " << greedyStr << endl;
    exit(-1);
  }

  /*
   * Plausibility check of option argument combinations
   */

  // do plausibility check of filter option arguments
  if (useAbandsArg) {
    if (useDesiredBandsArg or useDeltaArg) {
      cout << "Error: Incompatible Options. Use either --abands or"
           << " --desired_bands and --delta " << endl;
      print_short_help();
      exit(-1);
    }
  }
  if ((useDesiredBandsArg and !useDeltaArg) or
      (!useDesiredBandsArg and useDeltaArg)) {
    cout << "Error: Incompatible Options. Use --desired_bands in "
         << "combination with --delta " << endl;
    print_short_help();
    exit(-1);
  }

  // do plausibility check of ILP 1 and ILP 2 arguments
  if ((ilp_formulation == 2 or useNoOfPipelineStagesArg or useCostRegArg or
       useCostAddArg) and
      (ilp_formulation == 1 or useShiftMaxArg or useCoeffMaxArg or
       useIntFeasTolArg or useNoOfAddersMinArg or
       useNoOfAddersMaxArg or useObectiveArg)) {
    cout << "Error: Incompatible Options. ILP1 specific options cannot be used "
         << "simultaneously with ILP2 specific options." << endl;
    print_short_help();
    exit(-1);
  }

  /*
   * Parse filter spec
   */

  // parse fbands
  if (useFbandsArg) {
    char equal = fbands_strstr.get();
    if (equal != '=') {
      cout << "Error while parsing '=' in fBands: Incompatible input format";
      print_short_help();
      exit(-1);
    }

    char bandDelimiter;
    do {
      char leftBracket = fbands_strstr.get();
      if (leftBracket != '[') {
        cout << "Error while parsing '[' in fBands: Incompatible input format";
        print_short_help();
        exit(-1);
      }
      double leftBoundary;
      double rightBoundary;
      char boundaryDelimiter;
      fbands_strstr >> leftBoundary >> boundaryDelimiter >> rightBoundary;
      if (boundaryDelimiter != ',') {
        cout << "Error while parsing ',' in fBands: Incompatible input format";
        print_short_help();
        exit(-1);
      }
      char rightBracket = fbands_strstr.get();
      if (rightBracket != ']') {
        cout << "Error while parsing ']' in fBands: Incompatible input format";
        print_short_help();
        exit(-1);
      }
      double_pair_t fBandBoundaries = make_pair(leftBoundary, rightBoundary);
      Fbands.push_back(fBandBoundaries);
      bandDelimiter = fbands_strstr.get();

    } while (bandDelimiter == ',');
  }

  // parse abands
  if (useAbandsArg) {
    char equal = abands_strstr.get();
    if (equal != '=') {
      cout << "Error while parsing '=' in aBands: Incompatible input format";
      print_short_help();
      exit(-1);
    }

    char bandDelimiter;
    do {
      char leftBracket = abands_strstr.get();
      if (leftBracket != '[') {
        cout << "Error while parsing '[' in aBands: Incompatible input format";
        print_short_help();
        exit(-1);
      }
      double lowerBoundary;
      double upperBoundary;
      char boundaryDelimiter;
      abands_strstr >> lowerBoundary >> boundaryDelimiter >> upperBoundary;
      if (boundaryDelimiter != ',') {
        cout << "Error while parsing ',' in aBands: Incompatible input format";
        print_short_help();
        exit(-1);
      }
      char rightBracket = abands_strstr.get();
      if (rightBracket != ']') {
        cout << "Error while parsing ']' in aBands: Incompatible input format";
        print_short_help();
        exit(-1);
      }
      double_pair_t aBandBoundaries = make_pair(lowerBoundary, upperBoundary);
      Abands.push_back(aBandBoundaries);
      bandDelimiter = abands_strstr.get();

    } while (bandDelimiter == ',');
  }

  // parse custom gain
  if (useCustomGain) {
    char equal = gain_strstr.get();
    if (equal != '=') {
      cout << "Error while parsing '=' in gain: Incompatible input format";
      print_short_help();
      exit(-1);
    }
    char leftBracket = gain_strstr.peek();
    if (leftBracket == '[') {
      useGainBounds = true;
      leftBracket = gain_strstr.get();
      double leftGain;
      double rightGain;
      char boundaryDelimiter;
      gain_strstr >> leftGain >> boundaryDelimiter >> rightGain;
      if (boundaryDelimiter != ',') {
        cout << "Error while parsing ',' in gain_init: Incompatible input format";
        print_short_help();
        exit(-1);
      }
      char rightBracket = gain_strstr.get();
      if (rightBracket != ']') {
        cout << "Error while parsing ']' in gain_init: Incompatible input format";
        print_short_help();
        exit(-1);
      }

      if (leftGain <=0 || rightGain <= 0) {
        cout << "Error while parsing bounds in gain_init: only positive values allowed";
        print_short_help();
        exit(-1);
      }
      if (leftGain <= rightGain)
        gainBounds = make_pair(leftGain, rightGain);
      else 
        gainBounds = make_pair(rightGain, leftGain);
    } else {
      gain_strstr >> gainInit;
      if (gainInit <= 0.0) {
        cout << "Error while parsing the fixed gain: only positive values allowed";
        print_short_help();
        exit(-1);
      }
    }
  } else {
    gainInit = 1.0;
  }

  // parse desired bands
  if (useDesiredBandsArg) {
    char equal = desired_bands_strstr.get();
    if (equal != '=') {
      cout << "Error while parsing '=' in desired bands: Incompatible input "
              "format";
      print_short_help();
      exit(-1);
    }

    char bandDelimiter;
    do {

      double desiredBand;
      desired_bands_strstr >> desiredBand;
      desiredBands.push_back(desiredBand);
      bandDelimiter = desired_bands_strstr.get();

    } while (bandDelimiter == ',');
  }

  // parse delta
  bool useExplicitDelta = false;

  if (useDeltaArg) {
    char equal = delta_strstr.get();
    if (equal != '=') {
      cout << "Error while parsing '=' in delta: Incompatible input format";
      print_short_help();
      exit(-1);
    }

    char firstChar = delta_strstr.peek();
    if (firstChar == '[')
    // parse explicit declaration of delta
    {
      useExplicitDelta = true;
      char bandDelimiter;
      do {
        char leftBracket = delta_strstr.get();
        if (leftBracket != '[') {
          cout << "Error while parsing '[' in delta: Incompatible input format";
          print_short_help();
          exit(-1);
        }
        double downwardDelta;
        double upwardDelta;
        char boundaryDelimiter;
        delta_strstr >> downwardDelta >> boundaryDelimiter >> upwardDelta;
        if (boundaryDelimiter != ',') {
          cout << "Error while parsing ',' in delta: Incompatible input format";
          print_short_help();
          exit(-1);
        }
        char rightBracket = delta_strstr.get();
        if (rightBracket != ']') {
          cout << "Error while parsing ']' in delta: Incompatible input format";
          print_short_help();
          exit(-1);
        }
        double_pair_t bandDeltas = make_pair(downwardDelta, upwardDelta);
        delta.push_back(bandDeltas);
        bandDelimiter = delta_strstr.get();

      } while (bandDelimiter == ',');

    } else {
      // parse alternative delta
      char deltaDelimiter;
      do {

        double bandDelta;
        delta_strstr >> bandDelta;
        deltaAlternative.push_back(bandDelta);
        deltaDelimiter = delta_strstr.get();

      } while (deltaDelimiter == ',');
    }
  }

  // generating abands
  for (int i = 0; i < desiredBands.size(); i++) {
    double lowerBoundary;
    double upperBoundary;

    if (useExplicitDelta) // use explicit delta
    {
      lowerBoundary = desiredBands[i] + delta[i].first;
      upperBoundary = desiredBands[i] + delta[i].second;
    } else // use alternative delta
    {
      double tmpDelta;
      if (deltaAlternative.size() == 1) {
        tmpDelta = deltaAlternative[0];
      } else {
        tmpDelta = deltaAlternative[i];
      }
      lowerBoundary = desiredBands[i] - tmpDelta;
      upperBoundary = desiredBands[i] + tmpDelta;
    }
    double_pair_t aBandBoundaries = make_pair(lowerBoundary, upperBoundary);
    Abands.push_back(aBandBoundaries);
  }

  // default Fbands definition
  if (Fbands.empty()) {
    Fbands = {make_pair(0.0, 0.3), make_pair(0.5, 1.0)};
  }
  // default Abands definition
  if (Abands.empty()) {
    // test problem for fixed gain (runtime: < 1 second)
    Abands = {make_pair(1.8671875, 1.921875),
              make_pair(-1.3671875e-2, 1.3671875e-2)};
  }

  if (useExtraPoints) {
    char extraDelimiter;
    do {
      double extraPoint;
      extra_strstr >> extraPoint;
      extraGridPoints.push_back(extraPoint);
      extraDelimiter = extra_strstr.get();

    } while (extraDelimiter == ',');
  }

  if (useCoeffInit) {
    char extraDelimiter;
    do {
      int coeff;
      coeff_init_strstr >> coeff;
      coeffInit.push_back(coeff);
      extraDelimiter = coeff_init_strstr.get();

    } while (extraDelimiter == ',');
  }

  // sanity checks for the filter specification data
  for (int i = 0; i < Fbands.size(); ++i) {

    if (Fbands[i].first > Fbands[i].second) {
      cout << "Error: Invalid frequency interval [" << Fbands[i].first << ", "
           << Fbands[i].second << "]." << endl;
      exit(-1);
    }

    if (i > 0 and Fbands[i - 1].second > Fbands[i].first) {
      cout << "Error: Non disjoint frequency intervals [" << Fbands[i - 1].first
           << ", " << Fbands[i - 1].second << "] and [" << Fbands[i].first
           << ", " << Fbands[i].second << "]." << endl;
      exit(-1);
    }

    if (Fbands.size() != Abands.size()) {
      cout << "Error: The number of frequency bands does not "
           << "correspond to the number of amplitude constraints." << endl;
      exit(-1);
    }
  }

  // General Output
  IF_VERBOSE(1) cout << "verbose=" << global_verbose << endl;
  IF_VERBOSE(1) cout << "wordlength=" << wordlength << endl;
  IF_VERBOSE(1) cout << "noOfCoefficients=" << noOfCoefficients << endl;
  IF_VERBOSE(1) cout << "ftype=" << ir_type + 1 << endl;
  IF_VERBOSE(1)
  cout << "grid=" << (!useUniformFrequencyGrid ? "non-" : "") << "uniform"
       << endl;
  IF_VERBOSE(1) cout << "gridPoints=" << grid_points << endl;
  IF_VERBOSE(1) cout << "solver=" << preferredSolver << endl;
  IF_VERBOSE(1) cout << "formulation=" << ilp_formulation << endl;
  IF_VERBOSE(1) cout << "timeout=" << timeout << endl;
  IF_VERBOSE(1) cout << "threads=" << threads << endl;
  IF_VERBOSE(1) cout << "use relaxations=" << useRelaxation << endl;
  if(useCustomGain) {
    if(useGainBounds) {
      IF_VERBOSE(1) cout << "gain_bounds=[" << gainBounds.first << ", "
                         << gainBounds.second << "]" << endl; 
    } else {
      IF_VERBOSE(1) cout << "gain=" << gainInit << endl;
    }
  } else {
    IF_VERBOSE(1) cout << "gain=1" << endl;
  }

  // Filter Spec Output
  IF_VERBOSE(1) {
    // print Fbands
    cout << "fBands: ";
    int fBands_delimiter = 0;
    for (double_pair_t boundary : Fbands) {
      cout << "[" << boundary.first << "," << boundary.second << "]";
      fBands_delimiter++;
      if (fBands_delimiter != Fbands.size()) {
        cout << ",";
      }
    }
    cout << endl;
  }

  IF_VERBOSE(1) {
    // print Abands
    cout << "aBands: ";
    int aBands_delimiter = 0;
    for (double_pair_t boundary : Abands) {
      cout << "[" << boundary.first << "," << boundary.second << "]";
      aBands_delimiter++;
      if (aBands_delimiter != Abands.size()) {
        cout << ",";
      }
    }
    cout << endl;
  }

  IF_VERBOSE(1) {
    // print desired bands
    cout << "desired bands: ";
    int desiredBands_delimiter = 0;
    for (double band : desiredBands) {
      cout << band;
      desiredBands_delimiter++;
      if (desiredBands_delimiter != desiredBands.size()) {
        cout << ",";
      }
    }
    cout << endl;
  }

  IF_VERBOSE(1) {
    // print desired deltas
    if (useExplicitDelta) {
      cout << "explicit deltas: ";
      int delta_delimiter = 0;
      for (double_pair_t bandDeltas : delta) {
        cout << "[" << bandDeltas.first << "," << bandDeltas.second << "]";
        delta_delimiter++;
        if (delta_delimiter != delta.size()) {
          cout << ",";
        }
      }
    } else {
      // print alternative delta
      cout << "alternative deltas: ";
      int delta_delimiter = 0;
      for (double bandDelta : deltaAlternative) {
        cout << bandDelta;
        delta_delimiter++;
        if (delta_delimiter != deltaAlternative.size()) {
          cout << ",";
        }
      }
      cout << endl;
    }

    cout << endl;
  }

  // ILP2 Output
  IF_VERBOSE(1) cout << "stages=" << no_of_pipeline_stages << endl;
  IF_VERBOSE(1) cout << "cost_add=" << cost_add << endl;
  IF_VERBOSE(1) cout << "cost_reg=" << cost_reg << endl;

  // ILP1 Output
  IF_VERBOSE(1) cout << "max shift=" << shiftMax << endl;
  IF_VERBOSE(1) cout << "max coeff=" << coeffMax << endl;
  IF_VERBOSE(1) cout << "big M=" << bigM << endl;
  IF_VERBOSE(1) cout << "int feas tol=" << intFeasTol << endl;
  IF_VERBOSE(1)
  cout << "use indicator constraints=" << useIndicatorConstraints << endl;
  IF_VERBOSE(1) cout << "no of adders min=" << noOfAddersMin << endl;
  IF_VERBOSE(1) cout << "no of adders max=" << noOfAddersMax << endl;
  IF_VERBOSE(1) cout << "objective=" << objectiveStr << endl;
  IF_VERBOSE(1) cout << "ILP1objective=" << ILP1objective << endl;

  // cout << "no of outputs=" << noOfOutputs << endl; //TODO: fix this one, see
  // omcm code

  /*
   * Save settings in a settings file
   */
  stringstream ss;
  ss << rootOutputName << ".settings";
  std::ofstream settings(ss.str());

  // General Settings
  settings << "General settings:" << endl;
  settings << "verbose=" << global_verbose << endl;
  settings << "wordlength=" << wordlength << endl;
  settings << "noOfCoefficients=" << noOfCoefficients << endl;
  settings << "ftype=" << ir_type + 1 << endl;
  settings << "grid=" << (!useUniformFrequencyGrid ? "non-" : "") << "uniform"
           << endl;
  settings << "gridPoints=" << grid_points << endl;
  settings << "solver=" << preferredSolver << endl;
  settings << "formulation=" << ilp_formulation << endl;
  settings << "timeout=" << timeout << endl;
  settings << "threads=" << threads << endl;
  settings << "use ternary adders=" << useTernaryAdder << endl;
  settings << "use relaxations=" << useRelaxation << endl;
  if(useCustomGain) {
    if(useGainBounds) {
      settings << "gain_bounds=[" << gainBounds.first << ", "
                         << gainBounds.second << "]" << endl; 
    } else {
      settings << "gain=" << gainInit << endl;
    }
  } else {
    settings << "gain=1" << endl;
  }
  settings << endl;

  // Filter Spec settings
  settings << "Filter spec settings:" << endl;

  // print Fbands
  settings << "fBands: ";
  int fBands_delimiter = 0;
  for (double_pair_t boundary : Fbands) {
    settings << "[" << boundary.first << "," << boundary.second << "]";
    fBands_delimiter++;
    if (fBands_delimiter != Fbands.size()) {
      settings << ",";
    }
  }
  settings << endl;

  // print Abands
  settings << "aBands: ";
  int aBands_delimiter = 0;
  for (double_pair_t boundary : Abands) {
    settings << "[" << boundary.first << "," << boundary.second << "]";
    aBands_delimiter++;
    if (aBands_delimiter != Abands.size()) {
      settings << ",";
    }
  }
  settings << endl;

  // print desired bands
  settings << "desired bands: ";
  int desiredBands_delimiter = 0;
  for (double band : desiredBands) {
    settings << band;
    desiredBands_delimiter++;
    if (desiredBands_delimiter != desiredBands.size()) {
      settings << ",";
    }
  }
  settings << endl;

  // print desired deltas
  if (useExplicitDelta) {
    settings << "explicit deltas: ";
    int delta_delimiter = 0;
    for (double_pair_t bandDeltas : delta) {
      settings << "[" << bandDeltas.first << "," << bandDeltas.second << "]";
      delta_delimiter++;
      if (delta_delimiter != delta.size()) {
        settings << ",";
      }
    }
  } else {
    // print alternative delta
    settings << "alternative deltas: ";
    int delta_delimiter = 0;
    for (double bandDelta : deltaAlternative) {
      settings << bandDelta;
      delta_delimiter++;
      if (delta_delimiter != deltaAlternative.size()) {
        settings << ",";
      }
    }
    settings << endl;
  }

  settings << endl;

  // ILP2 Settings
  settings << "ILP2 settings:" << endl;
  settings << "stages=" << no_of_pipeline_stages << endl;
  settings << "cost_add=" << cost_add << endl;
  settings << "cost_reg=" << cost_reg << endl;
  settings << endl;

  // ILP1 Settings
  settings << "ILP1 settings:" << endl;
  settings << "max shift=" << shiftMax << endl;
  settings << "max coeff=" << coeffMax << endl;
  settings << "int feas tol=" << intFeasTol << endl;
  settings << "no of adders min=" << noOfAddersMin << endl;
  settings << "no of adders max=" << noOfAddersMax << endl;
  settings << "objective=" << objectiveStr << endl;
  settings << "ILP1objective=" << ILP1objective << endl;

  settings << "use indicator constraints=" << useIndicatorConstraints << endl;
  settings << "big M=" << bigM << endl;

  settings.close();

  std::list<std::string> solverWishList;
  if (!preferredSolver.empty())
    solverWishList.push_back(preferredSolver);
  else
    solverWishList = {"Gurobi", "CPLEX", "SCIP", "LPSolve"};

  IF_VERBOSE(1) {
    cout << "solverWishList=";
    for (auto s : solverWishList) {
      cout << s << " ";
    }
    cout << endl;
  }

  FIROpt *firopt;

  switch (ilp_formulation) {
  case 1:

    firopt = new FIROpt_ILP_1(
        solverWishList, threads, timeout, noOfCoefficients, ir_type,
        rootOutputName, Fbands, Abands, gainBounds, wordlength, grid_points,
        shiftMax, coeffMax, bigM, intFeasTol, useIndicatorConstraints,
        useRelaxation, noOfAddersMin, noOfAddersMax,
        ILP1objective);

    firopt->useUniformFrequencyGrid = useUniformFrequencyGrid;
    firopt->useFixedGain = !useGainBounds;
    firopt->objective = objective;

    break;

  case 2:

    firopt = new FIROpt_ILP_2(solverWishList, threads, timeout,
                              noOfCoefficients, ir_type, rootOutputName, Fbands,
                              Abands, gainBounds, wordlength, grid_points);

    // set the parameters:
    firopt->band_tolerance = 1e-6;
    firopt->setCostAdder(cost_add);
    firopt->setCostRegister(cost_reg);
    firopt->setNoOfPipelineStages(no_of_pipeline_stages);
    firopt->useUniformFrequencyGrid = useUniformFrequencyGrid;
    firopt->useFixedGain = !useGainBounds;
    firopt->setUseRelaxation(useRelaxation);
    firopt->objective = objective;
    firopt->useIndicatorConstraints = useIndicatorConstraints;
    firopt->bigM = bigM;
  }

  if (useExtraPoints) {
    firopt->extraPoints = extraGridPoints;
  }

  firopt->coeffInit = coeffInit;
  firopt->gainInit = gainInit;
  firopt->adderGraphInit = adderGraphInit;
  firopt->setUseTernaryAdder(useTernaryAdder);

  if (useGreedy)
    firopt->greedy_solve();
  else
    firopt->solve();

  int exitStatus = firopt->printSolution();

  return exitStatus;
}