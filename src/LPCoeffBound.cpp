//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#include "LPCoeffBound.h"

LPCoeffBound::LPCoeffBound(std::list<std::string> solverWishList, int threads,
                           int timeout, int noOfCoefficients, IRtype ir_type,
                           int wordlength, vector<vector<double>> omega,
                           int no_bands, vector<pair<double, double>> Abands,
                           double gain_init, pair<double, double> gain_bounds)
    : noOfCoefficients(noOfCoefficients), ir_type(ir_type),
      wordlength(wordlength), omega(omega), solverWishList(solverWishList),
      threads(threads), timeout(timeout), _no_bands(no_bands),
      gain_init(gain_init), gain_bounds(gain_bounds) {

  if (noOfCoefficients < 2) {
    cout << "Filter order must be at least 2 but is set to " << noOfCoefficients
         << "\n";
    return;
  }

  this->A_bands = Abands;

  // resize the bounds container
  h_bounds.resize(noOfCoefficients);

  // resize and fill the container for the variables
  _h_m.resize(noOfCoefficients);
  // add the implicit bound on the coefficient values
  double sizeBound = pow(2, wordlength) - 1;
  for (int i = 0; i < noOfCoefficients; ++i) {
    stringstream strstra;
    strstra << "h_" << i;
    _h_m[i] = ScaLP::newRealVariable(strstra.str(), -sizeBound, sizeBound);
  }

  _create_basis();
  _create_IR_bounds();
  if (gain_init < 0.0)
    _update_gain_bounds();
  // Create constraints C1 and C2
  _create_constraint_C1();
}

void LPCoeffBound::_create_basis() {
  _basis.resize(noOfCoefficients);
  switch (ir_type) {
  case SYM_EVEN: {
    IF_VERBOSE(5) cout << "filter type: SYM_EVEN" << endl;
    _basis[0] = [](double omega) -> double { return 1.0; };
    for (int_t i{1u}; i < noOfCoefficients; ++i)
      _basis[i] = [i](double omega) -> double {
        return 2.0 * cos(omega * M_PI * i);
      };
    break;
  }
  case SYM_ODD: {
    IF_VERBOSE(5) cout << "filter type: SYM_ODD" << endl;
    for (int_t i{0u}; i < noOfCoefficients; ++i)
      _basis[i] = [i](double omega) -> double {
        return 2.0 * cos(omega * M_PI * (i + .5));
      };
    break;
  }
  case ASYM_EVEN: {
    IF_VERBOSE(5) cout << "filter type: ASYM_EVEN" << endl;
    for (int_t i{0u}; i < noOfCoefficients; ++i)
      _basis[i] = [i](double omega) -> double {
        return 2.0 * sin(omega * M_PI * (i + 1));
      };
    break;
  }
  default: { // ASYM_ODD
    IF_VERBOSE(5) cout << "filter type: ASYM_ODD" << endl;
    for (int_t i{0u}; i < noOfCoefficients; ++i)
      _basis[i] = [i](double omega) -> double {
        return 2.0 * sin(omega * M_PI * (i + .5));
      };
    break;
  }
  }
}

void LPCoeffBound::_create_IR_bounds() {
  _bounds.resize(_no_bands);
  for (int i = 0; i < _no_bands; ++i) {
    _bounds[i] = [i](double omega, double A_bound) -> double {
      return A_bound;
    };
  }
}

int LPCoeffBound::compute_bounds() {

  int i;
  for (i = 0; i < noOfCoefficients; ++i) {
    // Solving the minimization problem for the coefficient _h_m, m=0...M-1
    ScaLP::Solver *s_min = new ScaLP::Solver(solverWishList);
    s_min->threads = this->threads;
    s_min->timeout = this->timeout;

    s_min->setObjective(ScaLP::minimize(_h_m[i]));

    for (auto constraint : _terms_C1) {
      IF_VERBOSE(5) cout << *constraint << endl;
      *s_min << *constraint;
    }

    // s_min->quiet = false;
    ScaLP::status status_min = s_min->solve();

    if (status_min != ScaLP::status::OPTIMAL &&
        status_min != ScaLP::status::FEASIBLE) {
      cout << "--------> COULD NOT SOLVE THE LP PROBLEM (MIN) FOR THE "
              "COEFFICIENT h_"
           << i << endl
           << "--------> Status is: " << status_min << endl;
      exit(16);
    }

    ScaLP::Result res_min = s_min->getResult();
    h_bounds[i].first = (int)(ceil(res_min.objectiveValue));

    delete s_min;

    // Solving the maximization problem for the coefficient _h_m, m=0,...,M-1
    ScaLP::Solver *s_max = new ScaLP::Solver(solverWishList);
    s_max->threads = this->threads;
    s_max->timeout = this->timeout;

    s_max->setObjective(
        ScaLP::maximize(_h_m[i])); // ToDo: recycle old solver object!

    for (auto constraint : _terms_C1) {
      *s_max << *constraint;
    }

    // s_max->quiet = false;
    ScaLP::status status_max = s_max->solve();

    if (status_max != ScaLP::status::OPTIMAL &&
        status_max != ScaLP::status::FEASIBLE) {
      cout << "--------> COULD NOT SOLVE THE LP PROBLEM (MAX) FOR THE "
              "COEFFICIENT h_"
           << i << endl
           << "--------> Status is: " << status_max << endl;
      exit(16);
    }
    ScaLP::Result res_max = s_max->getResult();
    h_bounds[i].second = (int)(floor(res_max.objectiveValue));
    std::cout << "Bounds for coefficient " << i << " is " << h_bounds[i].first
              << " ... " << h_bounds[i].second << std::endl;

    delete s_max;
  }

  return 1;
}

void LPCoeffBound::_create_constraint_C1() {

  gain = ScaLP::newRealVariable("gain", gain_bounds.first, gain_bounds.second);

  for (int band_i = 0; band_i < _no_bands; band_i++) {
    for (int j = 0; j < omega[band_i].size(); j++) {

      ScaLP::Term term_c2(0);
      double lower_bound =
          _bounds[band_i](omega[band_i][j], A_bands[band_i].first);
      double upper_bound =
          _bounds[band_i](omega[band_i][j], A_bands[band_i].second);

      for (int m = 0; m < noOfCoefficients; ++m) {
        term_c2 = term_c2 + _basis[m](omega[band_i][j]) * _h_m[m];
      }
      if (gain_init > 0.0) {
        _terms_C1.push_back(
            new ScaLP::Constraint(
              lower_bound * gain_init <= term_c2 <= upper_bound * gain_init
              ));
      } else {
        _terms_C1.push_back(
            new ScaLP::Constraint(0 <= term_c2 - lower_bound * gain));
        _terms_C1.push_back(
            new ScaLP::Constraint(term_c2 - upper_bound * gain <= 0));
      }
    }
  }
}

void LPCoeffBound::_update_gain_bounds() {
  vector<ScaLP::Variable> _h_m_b;
  list<ScaLP::Constraint *> _terms_C1_b;
  // resize and fill the container for the variables
  _h_m_b.resize(noOfCoefficients);
  // add the implicit bound on the coefficient values
  double sizeBound = pow(2, wordlength) - 1;
  for (int i = 0; i < noOfCoefficients; ++i) {
    stringstream strstra;
    strstra << "h_" << i;
    _h_m_b[i] = ScaLP::newRealVariable(strstra.str(), -sizeBound, sizeBound);
  }

  ScaLP::Variable gain_b = ScaLP::newRealVariable("gain", gain_bounds.first, gain_bounds.second);
  for (int band_i = 0; band_i < _no_bands; band_i++) {
    for (int j = 0; j < omega[band_i].size(); j++) {

      ScaLP::Term term_c2(0);
      double lower_bound =
          _bounds[band_i](omega[band_i][j], A_bands[band_i].first);
      double upper_bound =
          _bounds[band_i](omega[band_i][j], A_bands[band_i].second);

      for (int m = 0; m < noOfCoefficients; ++m) {
        term_c2 = term_c2 + _basis[m](omega[band_i][j]) * _h_m_b[m];
      }
      _terms_C1_b.push_back(
          new ScaLP::Constraint(0 <= term_c2 - lower_bound * gain_b));
      _terms_C1_b.push_back(
          new ScaLP::Constraint(term_c2 - upper_bound * gain_b <= 0));
    }
  }

  ScaLP::Solver *s_min = new ScaLP::Solver(solverWishList);
  s_min->threads = this->threads;
  s_min->timeout = this->timeout;

  s_min->setObjective(ScaLP::minimize(gain_b));
  for(auto constraint : _terms_C1_b) {
    IF_VERBOSE(5) cout << *constraint << endl;
    *s_min << *constraint;
  }

  ScaLP::status status_min = s_min->solve();
  if (status_min != ScaLP::status::OPTIMAL &&
      status_min != ScaLP::status::FEASIBLE) {
        cout << "--------> COULD NOT SOLVE THE LP PROBLEM (MIN) FOR THE "
                "COEFFICIENT gain\n"
             << "--------> Status is: " << status_min << endl;
        exit(16);   // TODO: maybe implement an exception mechanism for all the 
                    // 'extraordinary' exit situations
  }

  ScaLP::Result res_min = s_min->getResult();
  if (gain_bounds.first < res_min.objectiveValue)
    gain_bounds.first = res_min.objectiveValue;

  delete s_min;

  ScaLP::Solver *s_max = new ScaLP::Solver(solverWishList);
  s_max->threads = this->threads;
  s_max->timeout = this->timeout;

  s_max->setObjective(ScaLP::maximize(gain_b));
  for(auto constraint : _terms_C1_b) {
    IF_VERBOSE(5) cout << *constraint << endl;
    *s_max << *constraint;
  }

  ScaLP::status status_max = s_max->solve();
  if (status_max != ScaLP::status::OPTIMAL &&
      status_max != ScaLP::status::FEASIBLE) {
        cout << "--------> COULD NOT SOLVE THE LP PROBLEM (MIN) FOR THE "
                "COEFFICIENT gain\n"
             << "--------> Status is: " << status_max << endl;
        exit(16);   // TODO: maybe implement an exception mechanism for all the 
                    // 'extraordinary' exit situations
  }

  ScaLP::Result res_max = s_max->getResult();
  if(gain_bounds.second > res_max.objectiveValue)
    gain_bounds.second = res_max.objectiveValue;

  delete s_max;
  cout << "Updated gain bounds to [" << gain_bounds.first << ", "
       << gain_bounds.second << "]\n";
}