//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

//    This file is adapted from the PAGSuite project, a suite of optimization
//    tools to optimize pipelined adder graphs. It is developed at the
//    University of Kassel and maintained by Martin Kumm
//    (martin.kumm@informatik.hs-fulda.de). You can use and/or modify it for
//    research purposes, for commercial use, please ask for a license. For more
//    information please visit http://www.uni-kassel.de/go/pagsuite.

#include "csd.h"
#include "debug.h"
#include "log2_64.h"

#include <algorithm>

void dec2bin(int_t x, sd_t *csd) {
  IF_VERBOSE(7) std::cout << "\n" << x << " in csd\n";
  if (x < 0) {
    x *= -1;
  } // this is necessary because of the vector matrix multiplication,
    // which have to work with negative numbers in a specific way.
  if (x != 0) {
    while (x != 0) {
      csd->push_back(x & 1);
      x = x >> 1;
    }
  } else {
    csd->push_back(0);
  }
}

int csd_replace(sd_t *csd, bool negativ) {
  bool replace = false;
  sd_t::iterator iter;
  sd_t::iterator end_iter = csd->end();
  for (iter = csd->begin(); iter != end_iter; ++iter) {
    if (*iter == 1) {
      if (replace) {
        *iter = 0;
      } else if (iter + 1 != end_iter) {
        if (*(iter + 1) == 1) {
          *iter = -1;
          replace = true;
        }
      }
    } else if (replace && (*iter == 0)) {
      *iter = 1;
      return 0;
    }
  }
  if (replace) {
    csd->push_back(1);
  }

  if (negativ) // for negativ numbers in vector matrix multiplication
  {
    end_iter = csd->end();
    for (iter = csd->begin(); iter != end_iter; ++iter) {
      if (*iter == 1) {
        *iter = -1;
      } else if (*iter == -1) {
        *iter = 1;
      }
    }
  }
  return 1;
}

int dec2csd(int_t x, sd_t *csd) {
  bool negativ;
  if (x < 0) {
    negativ = true;
  } else {
    negativ = false;
  }

  dec2bin(x, csd);
  IF_VERBOSE(7) cout << x << ": bin=" << *csd << endl;

  while (!csd_replace(csd, negativ))
    ;

  IF_VERBOSE(7) cout << x << ": csd=" << *csd << endl;
  return 1;
}

int nonzeros(sd_t *sd) {
  sd_t::iterator iter;
  int nz = 0;
  for (iter = sd->begin(); iter != sd->end(); ++iter) {
    if (*iter != 0)
      nz++;
  }
  return nz;
}

int nonzeros(int_t x) {
  sd_t csd;
  dec2csd(x, &csd);
  return nonzeros(&csd);
}