//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

#pragma once

#include "FIROpt.h"

class FIROpt_Sparse : public FIROpt {
public:
  // constructor:
  FIROpt_Sparse(std::list<std::string> solverWishList, int threads = 0,
                int timeout = -1, int noOfCoefficients = 3,
                IRtype ir_type = SYM_EVEN,
                string outputRootName = "Sparse_model",
                double_pair_vec_t Fbands = {std::make_pair(0.0, 0.3),
                                            std::make_pair(0.5, 1.0)},
                double_pair_vec_t Abands = {std::make_pair(11, 20),
                                            std::make_pair(-7, 7)},
                double_pair_t gain_bounds = std::make_pair(0.66, 1.34),
                int wordlength = 3, int grid_points = 10, int shiftMax = -1,
                int coeffMax = -1, long bigM = -1, double intFeasTol = 1e-5,
                bool useIndicatorConstraints = false);

  void solve() override;
  void greedy_solve() override;
  int printSolution() override;

  // getters:
  int getShiftMax() { return shiftMax; };
  int getCoeffMax() { return coeffMax; };
  pair<bool, int> getOptimalityStatus();
  long getBigM() { return bigM; };
  double getIntFeasTol() { return intFeasTol; };
  bool getUseIndicatorConstraints() { return useIndicatorConstraints; };

  // setters:
  void setShiftMax(int shiftMax) { this->shiftMax = shiftMax; };
  void setCoeffMax(int coeffMax) { this->coeffMax = coeffMax; };
  void setBigM(long bigM) { this->bigM = bigM; };
  void setIntFeasTol(double intFeasTol) { this->intFeasTol = intFeasTol; };
  void setUseIndicatorConstraints(bool useIndicatorConstraints) {
    this->useIndicatorConstraints = useIndicatorConstraints;
  };

private:
  void initialize_variables(bool greedy = false);
  void initialize_constraints();

  double intFeasTol{1e-5};
  int shiftMax{-1};
  int shiftMin{0};
  long coeffMax = -1;

  std::vector<ScaLP::Variable>
      _h_m_is_zero; // for structural adder optimization
};
