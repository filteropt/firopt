//    FIRopt
//    Copyright (C) 2019  FilterOpt
//    Contributor(s): Martin KUMM (martin.kumm@informatik.hs-fulda.de)
//                    Anastasia VOLKOVA (anastasia.volkova@inria.fr)
//                    Silviu FILIP (silviu.filip@inria.fr)

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>

//    This file is adapted from the PAGSuite project, a suite of optimization
//    tools to optimize pipelined adder graphs. It is developed at the
//    University of Kassel and maintained by Martin Kumm
//    (martin.kumm@informatik.hs-fulda.de). You can use and/or modify it for
//    research purposes, for commercial use, please ask for a license. For more
//    information please visit http://www.uni-kassel.de/go/pagsuite.

#ifndef CSD_H
#define CSD_H

#include "debug.h"
#include "types.h"

inline int csd_nonzeros2(int_t x) {
  int N = 8 * sizeof(int_t);
  int one_cnt = 0;

  for (int i = 0; i < N - 1; i++) {
    if ((x >> i) & 1) {
      if (i < 2)
        one_cnt++;
      else if ((i == 2) || (i == N - 1)) {
        if (!(((x >> (i - 2)) & 0x07) == 0x07))
          one_cnt++;
      } else // 2 < i < N-1
      {
        if (!(((x >> (i - 2)) & 0x07) == 0x07) &&
            !(((x >> (i - 3)) & 0x1F) == 0x1B))
          one_cnt++;
      }
    }
  }
  IF_VERBOSE(7) cout << "csd_nonzeros(" << x << ")=" << one_cnt - 1 << endl;
  return one_cnt;
}

/*
  In CSD a 11..11 is mapped to 10..00(-1)
*/
inline int csd_nonzeros(int_t x) {
  IF_VERBOSE(7) cout << "csd_nonzeros(" << x << ")" << endl;
  int N = 8 * sizeof(int_t);
  int one_cnt = 0;

  int fourlsb;

  cout << "x=";
  for (int i = 0; i < N - 1; i++) {
    cout << ((x >> i) & 1);
  }
  cout << endl;

  cout << "   ";
  for (int i = 0; i < N - 1; i++) {
    x = x >> 1;
    fourlsb = (x & 15);
    if ((fourlsb == 2) || (fourlsb == 3) || (fourlsb == 6) || (fourlsb == 10) ||
        (fourlsb == 14)) {
      cout << "*";
      one_cnt++;
    } else {
      cout << " ";
    }
  }
  cout << endl;
  return one_cnt;
}

int dec2csd(int_t x, sd_t *csd);
int nonzeros(sd_t *csd);
int nonzeros(int_t x);

#endif // CSD_H